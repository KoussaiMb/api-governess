'use strict';

const chai = require('chai');
const expect = require('chai').expect;
chai.use(require('chai-http'));

module.exports = function(testData) {
  describe('\n-- CUSTOMER ENDPOINTS -- ', function () {
    describe('get all customers', function () {
      it('route: /governess/customer', function () {
        return chai.request(testData.apiUrl)
          .get('/v2/governess/customer')
          .set({'apiToken': testData.apiToken})
          .then(function (res) {
            expect(res).to.have.status(200);
            let result = JSON.parse(JSON.stringify(res.body));
            expect(result["data"]);
          });
      });
    });
    describe('get customer with id 1', function () {
      it('route: /governess/customer/1', function () {
        return chai.request(testData.apiUrl)
          .get('/v2/governess/customer/1')
          .set({'apiToken': testData.apiToken})
          .then(function (res) {
            expect(res).to.have.status(200);
            let result = JSON.parse(JSON.stringify(res.body));
            expect(result["data"]["id"]).equal(1);
          })
      });
    });
  });
};
