'use strict';

const chai = require('chai');
const expect = require('chai').expect;
chai.use(require('chai-http'));

module.exports = function(testData) {
  describe('\n-- INSPECTION ENDPOINTS --', function() {
    describe('get the first page of inspections', function () {
      it('route: /governess/inspection/page/1', function () {
        return chai.request(testData.apiUrl)
          .get('/v2/governess/inspection/page/1')
          .set({'apiToken': testData.apiToken})
          .send({"page": 1})
          .then(function (res) {
            expect(res).to.have.status(200);
            let result = JSON.parse(JSON.stringify(res.body));
            expect(result["data"]);
          });
      });
    });
    describe('get inspection with id 1', function () {
      it('route: /governess/inspection/1', function () {
        return chai.request(testData.apiUrl)
          .get('/v2/governess/inspection/1')
          .set({'apiToken': testData.apiToken})
          .then(function (res) {
            expect(res).to.have.status(200);
            let result = JSON.parse(JSON.stringify(res.body));
            expect(result["data"]["id"]).equal(1);
          });
      });
    });
    describe('post an inspection for the mission with id 1', function () {
      it('route: /governess/inspection', function () {
        return chai.request(testData.apiUrl)
          .post('/v2/governess/inspection/')
          .set({'apiToken': testData.apiToken})
          .send({
            "mission_id": 1,
            "average_rate": 75,
            "rates": [{"cardex_id": 1, "rate": 3}],
            "grooming": 2,
            "comments": "Commentaire"
          })
          .then(function (res) {
            expect(res).to.have.status(200);
          });
      });
    });
    describe('Search inspections', function () {
      it('route: /governess/inspection/search', function () {
        return chai.request(testData.apiUrl)
          .get('/v2/governess/inspection/search')
          .set({'apiToken': testData.apiToken,
		'clientName': '%a%'})
          .then(function (res) {
            expect(res).to.have.status(200);
          });
      });
    });
    describe('Search inspections with no criteria', function () {
      it('route: /governess/inspection/search', function () {
        return chai.request(testData.apiUrl)
          .get('/v2/governess/inspection/search')
          .set({'apiToken': testData.apiToken})
          .then(function (res) {
            expect(res).to.have.status(404);
          });
      });
    });
  });
};
