'use strict';

const chai = require('chai');
const expect = require('chai').expect;
chai.use(require('chai-http'));

module.exports = function(testData) {
  describe('\n-- EVENT ENDPOINTS -- ', function () {
    describe('get event with appointment type', function () {
      it('route: /governess/event?eventType=appointment', function () {
        return chai.request(testData.apiUrl)
            .get('/v2/governess/event?eventType=appointment')
            .set({'apiToken': testData.apiToken,
              'startDate': '2018-07-01',
              'endDate': '2018-09-01'})
            .then(function (res) {
              expect(res).to.have.status(200);
              let result = JSON.parse(JSON.stringify(res.body));
              expect(result["data"]);
            });
      });
    });
    describe('get event with wrong date', function () {
      it('route: /governess/event', function () {
        return chai.request(testData.apiUrl)
            .get('/v2/governess/event')
            .set({'apiToken': testData.apiToken,
              'startDate': 'wrongdate',
              'endDate': '2018-09-01'})
            .then(function (res) {
              expect(res).to.have.status(400);
            });
      });
    });
    describe('create event with missing parameters', function () {
      it('route: /governess/event', function () {
        return chai.request(testData.apiUrl)
            .post('/v2/governess/event')
            .set({'apiToken': testData.apiToken})
            .send({title: 'Test title',
              start_rec: '2018-02-27'
            }).then(function (res) {
              expect(res).to.have.status(422);
            });
      });
    });
    describe('update non existing event', function () {
      it('route: /governess/event/-1', function () {
        return chai.request(testData.apiUrl)
            .put('/v2/governess/event/-1')
            .set({'apiToken': testData.apiToken,
              'startDate': 'wrongdate',
              'endDate': '2018-09-01'})
            .send({title: 'Test title',
              start_rec: '2018-02-27'
            }).then(function (res) {
              expect(res).to.have.status(404);
            });
      });
    });
  });
};
