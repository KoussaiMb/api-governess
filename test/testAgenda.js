'use strict';

const chai = require('chai');
const expect = require('chai').expect;
chai.use(require('chai-http'));

module.exports = function(testData) {
  describe('\n-- AGENDA ENDPOINTS --', function() {
    describe('get all missions for monday in the agenda', function () {
      it('route: /governess/agenda/day/1', function () {
        return chai.request(testData.apiUrl)
          .get('/v2/governess/agenda/day/1')
          .set({'apiToken': testData.apiToken})
          .then(function (res) {
            expect(res).to.have.status(200);
            let result = JSON.parse(JSON.stringify(res.body));
            expect(result["data"]);
          })
      });
    });
    describe('get missions for today in the agenda', function () {
      it('route: /governess/agenda/today', function () {
        return chai.request(testData.apiUrl)
          .get('/v2/governess/agenda/today')
          .set({'apiToken': testData.apiToken})
          .then(function (res) {
            expect(res).to.have.status(200);
            let result = JSON.parse(JSON.stringify(res.body));
            expect(result["data"]);
          })
      });
    });
  });
};