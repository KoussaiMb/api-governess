'use strict';

module.exports = function(testData) {
  describe('\n\n--- GOVERNESS SERVICE TEST ---\n', function () {
    require('./testAddress.js')(testData);
    require('./testEvent.js')(testData);
    require('./testCustomer.js')(testData);
    require('./testInspection.js')(testData);
    require('./testMission.js')(testData);
    require('./testProvider.js')(testData);
    require('./testAgenda.js')(testData);
  });
};