'use strict';

const chai = require('chai');
const expect = require('chai').expect;
chai.use(require('chai-http'));

module.exports = function(testData) {
  describe('\n-- MISSION ENDPOINTS --', function() {
    describe('get all missions', function () {
      it('route: /governess/mission', function () {
        return chai.request(testData.apiUrl)
          .get('/v2/governess/mission')
          .set({'apiToken': testData.apiToken})
          .then(function (res) {
            expect(res).to.have.status(200);
            let result = JSON.parse(JSON.stringify(res.body));
            expect(result["data"]);
          });
      });
  });
    describe('get mission with id 1', function () {
      it('route: /governess/mission/1', function() {
        return chai.request(testData.apiUrl)
          .get('/v2/governess/mission/1')
          .set({'apiToken': testData.apiToken})
          .then(function (res) {
            expect(res).to.have.status(200);
            let result = JSON.parse(JSON.stringify(res.body));
            expect(result["data"]["id"]).equal(1);
          })
      });
    });
    describe('get all on going missions', function () {
      it('route: /governess/mission/ongoing', function() {
        return chai.request(testData.apiUrl)
          .get('/v2/governess/mission/ongoing')
          .set({'apiToken': testData.apiToken})
          .then(function (res) {
            expect(res).to.have.status(200);
            let result = JSON.parse(JSON.stringify(res.body));
            expect(result["data"]);
          })
      });
    });
  });
};
