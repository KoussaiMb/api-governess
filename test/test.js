'use strict';
const app = require('../app.js'); // Our app
const models = require('../models');
app.listen(3005);
let testData = {apiUrl: 'localhost:3005', apiToken: "lol"};

describe('--- GOVERNESS SERVICE TEST ---\n', function () {
    before(function(done) {
	models.auth.update({api_token: "0e39674ddd62f39a1705"},
			   {where: {user_id: 1}
			   }).then(token => {
			       testData.apiToken = "0e39674ddd62f39a1705";
			       done();
			   });
	// runs before all tests in this block
    });
    require('./testAddress.js')(testData);
    require('./testCustomer.js')(testData);
    require('./testInspection.js')(testData);
    require('./testEvent.js')(testData);
    require('./testMission.js')(testData);
    require('./testProvider.js')(testData);
    require('./testAgenda.js')(testData);
});
