'use strict';

const chai = require('chai');
const expect = require('chai').expect;
chai.use(require('chai-http'));

module.exports = function(testData) {
  describe('\n-- PROVIDER ENDPOINTS --', function() {
    describe('get all providers', function () {
      it('route: /governess/provider', function () {
        return chai.request(testData.apiUrl)
          .get('/v2/governess/provider')
          .set({'apiToken': testData.apiToken})
          .then(function (res) {
            expect(res).to.have.status(200);
            let result = JSON.parse(JSON.stringify(res.body));
            expect(result["data"]);
          });
      });
    });
    describe('get provider with id 1', function () {
      it('route: /governess/provider/1', function() {
        return chai.request(testData.apiUrl)
          .get('/v2/governess/provider/1')
          .set({'apiToken': testData.apiToken})
          .then(function (res) {
            expect(res).to.have.status(200);
            let result = JSON.parse(JSON.stringify(res.body));
            expect(result["data"]["id"]).equal(1);
          })
      });
    });
    describe('get missions of the current week of provider with id 1', function () {
      it('route: /governess/provider/1/week/current', function () {
        return chai.request(testData.apiUrl)
          .get('/v2/governess/provider/1/week/current')
          .set({'apiToken': testData.apiToken})
          .then(function (res) {
            expect(res).to.have.status(200);
            let result = JSON.parse(JSON.stringify(res.body));
            expect(result["data"]);
          });
      });
    });
    describe('get missions of the next week of provider with id 1', function () {
      it('route: /governess/provider/1/week/next', function () {
        return chai.request(testData.apiUrl)
          .get('/v2/governess/provider/1/week/next')
          .set({'apiToken': testData.apiToken})
          .then(function (res) {
            expect(res).to.have.status(200);
            let result = JSON.parse(JSON.stringify(res.body));
            expect(result["data"]);
          });
      });
    });
  });
};
