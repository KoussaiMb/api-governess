'use strict';

let models = require('../models');

const chai = require('chai');
const expect = require('chai').expect;
chai.use(require('chai-http'));

module.exports = function(testData) {
  describe('\n-- ADDRESSES ENDPOINTS --', function() {
    this.timeout(3000);
    before(function(done) {
      models.auth.update({api_token: "0e39674ddd62f39a1705"},
        {where: {user_id: 63}}).then(() => {
        testData.apiToken = "0e39674ddd62f39a1705";
        done();
      })
    });
    describe('get all addresses', function () {
      it('route: /governess/address', function () {
        return chai.request(testData.apiUrl)
          .get('/v2/governess/address')
          .set({'apiToken': testData.apiToken})
          .then(function (res) {
            expect(res).to.have.status(200);
            let result = JSON.parse(JSON.stringify(res.body));
            expect(result["data"]);
          });
      });
    });
    describe('get address with id 1', function () {
      it('route: /governess/address/1', function() {
        return chai.request(testData.apiUrl)
        .get('/v2/governess/address/1')
        .set({'apiToken': testData.apiToken})
        .then(function (res) {
          expect(res).to.have.status(200);
          let result = JSON.parse(JSON.stringify(res.body));
          expect(result["data"]["id"]).equal(1);
        })
      });
    });
    describe('get address planning, with id 1', function () {
      it('route: /governess/address/planning/1', function() {
        return chai.request(testData.apiUrl)
          .get('/v2/governess/address/planning/1')
          .set({'apiToken': testData.apiToken})
          .then(function (res) {
            expect(res).to.have.status(200);
          })
      });
    });
    describe('get address product localization, with id 1', function () {
      it('route: /governess/address/product/1', function() {
        return chai.request(testData.apiUrl)
          .get('/v2/governess/address/product/1')
          .set({'apiToken': testData.apiToken})
          .then(function (res) {
            expect(res).to.have.status(200);
          })
      });
    });
  });
};
