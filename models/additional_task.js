/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('additional_task', {
    id: {
      type: DataTypes.INTEGER(5).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    task_id: {
      type: DataTypes.INTEGER(5).UNSIGNED,
      allowNull: true,
      references: {
        model: 'task',
        key: 'id'
      }
    },
    mission_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
      references: {
        model: 'mission',
        key: 'id'
      }
    },
    task_time: {
      type: DataTypes.TIME,
      allowNull: true
    },
    deleted: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    }
  }, {
    tableName: 'additional_task'
  });
};
