/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('customer_details', {
    id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    customer_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
      references: {
        model: 'customer',
        key: 'id'
      }
    },
    animal_nb: {
      type: DataTypes.INTEGER(2),
      allowNull: true
    },
    child_nb: {
      type: DataTypes.INTEGER(2),
      allowNull: true
    },
    gift_preference: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    perfume_preference: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    housemaid_already: {
      type: DataTypes.STRING(3),
      allowNull: true
    },
    sport_interest: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    allergies: {
      type: DataTypes.STRING(25),
      allowNull: true
    },
    birthdate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    date_answer: {
      type: DataTypes.DATE,
      allowNull: true
    },
    deleted: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    }
  }, {
    tableName: 'customer_details'
  });
};
