/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('log_bo', {
    id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    user_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
      references: {
        model: 'user',
        key: 'id'
      }
    },
    function: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    error: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    date_create: {
      type: DataTypes.DATE,
      allowNull: true
    },
    file: {
      type: DataTypes.STRING(50),
      allowNull: true
    }
  }, {
    tableName: 'log_bo'
  });
};
