/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('payment_card', {
    id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    user_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
      references: {
        model: 'user',
        key: 'id'
      }
    },
    last_digits: {
      type: DataTypes.INTEGER(4),
      allowNull: true
    },
    date_expiration: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    main: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    payment_account: {
      type: DataTypes.STRING(50),
      allowNull: true,
      unique: true
    }
  }, {
    tableName: 'payment_card'
  });
};
