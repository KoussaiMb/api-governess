/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('promocode_user', {
		id: {
			type: DataTypes.INTEGER(10).UNSIGNED,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		user_id: {
			type: DataTypes.INTEGER(10).UNSIGNED,
			allowNull: true,
			references: {
				model: 'user',
				key: 'id'
			}
		},
		promocode_id: {
			type: DataTypes.INTEGER(5).UNSIGNED,
			allowNull: true,
			references: {
				model: 'promocode',
				key: 'id'
			}
		},
		occurrence: {
			type: DataTypes.INTEGER(5),
			allowNull: true,
			defaultValue: '1'
		},
		used: {
			type: DataTypes.INTEGER(5),
			allowNull: true,
			defaultValue: '0'
		},
		date_create: {
			type: DataTypes.DATE,
			allowNull: true
		},
		deleted: {
			type: DataTypes.INTEGER(1),
			allowNull: false,
			defaultValue: '0'
		}
	}, {
		tableName: 'promocode_user'
	});
};
