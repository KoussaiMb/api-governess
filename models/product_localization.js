/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('product_localization', {
    id: {
      type: DataTypes.INTEGER(5).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    address_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
      references: {
        model: 'address',
        key: 'id'
      }
    },
    lr_product_id: {
      type: DataTypes.INTEGER(5).UNSIGNED,
      allowNull: true,
      references: {
        model: 'list_reference',
        key: 'id'
      }
    },
    lr_room_id: {
      type: DataTypes.INTEGER(5).UNSIGNED,
      allowNull: true,
      references: {
        model: 'list_reference',
        key: 'id'
      }
    },
    description: {
      type: DataTypes.STRING(150),
      allowNull: true
    }
  }, {
    tableName: 'product_localization'
  });
};
