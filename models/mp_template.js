/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('mp_template', {
    id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(80),
      allowNull: true
    },
    min_area: {
      type: DataTypes.INTEGER(5).UNSIGNED,
      allowNull: true
    },
    max_area: {
      type: DataTypes.INTEGER(5).UNSIGNED,
      allowNull: true
    },
    ironing: {
      type: DataTypes.INTEGER(1),
      allowNull: true
    },
    l_recurrence_id: {
      type: DataTypes.INTEGER(5).UNSIGNED,
      allowNull: true,
      references: {
        model: 'list',
        key: 'id'
      }
    },
    childs: {
      type: DataTypes.INTEGER(1),
      allowNull: true
    },
    deleted: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    }
  }, {
    tableName: 'mp_template'
  });
};
