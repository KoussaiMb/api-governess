/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('log_payment_error', {
    id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    user_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
      references: {
        model: 'user',
        key: 'id'
      }
    },
    error_type: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    message: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    param: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    code: {
      type: DataTypes.STRING(3),
      allowNull: true
    },
    date_create: {
      type: DataTypes.DATE,
      allowNull: true
    },
    device: {
      type: DataTypes.STRING(50),
      allowNull: true
    }
  }, {
    tableName: 'log_payment_error'
  });
};
