/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('address_cluster', {
		id: {
			type: DataTypes.INTEGER(10),
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		address_id: {
			type: DataTypes.INTEGER(10).UNSIGNED,
			allowNull: false,
			references: {
				model: 'address',
				key: 'id'
			},
			unique: true
		},
		customer_id: {
			type: DataTypes.INTEGER(10).UNSIGNED,
			allowNull: false,
			references: {
				model: 'customer',
				key: 'id'
			}
		},
		cluster_id: {
			type: DataTypes.INTEGER(5).UNSIGNED,
			allowNull: true,
			references: {
				model: 'cluster',
				key: 'id'
			}
		}
	}, {
		tableName: 'address_cluster'
	});
};
