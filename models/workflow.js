/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('workflow', {
    id: {
      type: DataTypes.INTEGER(3).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    step: {
      type: DataTypes.INTEGER(3).UNSIGNED,
      allowNull: false,
      defaultValue: '1'
    },
    step_name: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    url: {
      type: DataTypes.STRING(80),
      allowNull: true
    }
  }, {
    tableName: 'workflow'
  });
};
