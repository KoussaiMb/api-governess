/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('auth', {
    id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    user_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
      references: {
        model: 'user',
        key: 'id'
      }
    },
    api_token: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    api_token_expire: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'auth'
  });
};
