/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('metro_paris', {
    id: {
      type: DataTypes.INTEGER(5).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    lattitude: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    longitude: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    arret: {
      type: DataTypes.STRING(54),
      allowNull: true
    },
    ligne: {
      type: DataTypes.STRING(48),
      allowNull: true
    },
    type: {
      type: DataTypes.STRING(29),
      allowNull: true
    }
  }, {
    tableName: 'metro_paris'
  });
};
