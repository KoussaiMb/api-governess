/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('log_form_order', {
    id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    promocode_id: {
      type: DataTypes.INTEGER(5).UNSIGNED,
      allowNull: true,
      references: {
        model: 'promocode',
        key: 'id'
      }
    },
    user_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
      references: {
        model: 'user',
        key: 'id'
      }
    },
    date_create: {
      type: DataTypes.DATE,
      allowNull: true
    },
    oldPrice: {
      type: DataTypes.INTEGER(6),
      allowNull: true
    },
    newPrice: {
      type: DataTypes.INTEGER(6),
      allowNull: true
    }
  }, {
    tableName: 'log_form_order'
  });
};
