/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('list_reference', {
    id: {
      type: DataTypes.INTEGER(5).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    list_name: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    value: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    url: {
      type: DataTypes.STRING(80),
      allowNull: true
    }
  }, {
    tableName: 'list_reference'
  });
};
