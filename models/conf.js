/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('conf', {
    id: {
      type: DataTypes.INTEGER(5).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(40),
      allowNull: true
    },
    value: {
      type: DataTypes.STRING(300),
      allowNull: true
    }
  }, {
    tableName: 'conf'
  });
};
