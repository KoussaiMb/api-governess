/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('mp_cardex', {
    id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    l_maintenanceType_id: {
      type: DataTypes.INTEGER(5).UNSIGNED,
      allowNull: true,
      references: {
        model: 'list',
        key: 'id'
      }
    },
    deleted: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    }
  }, {
    tableName: 'mp_cardex'
  });
};
