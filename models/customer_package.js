/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('customer_package', {
		id: {
			type: DataTypes.INTEGER(5).UNSIGNED,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		name: {
			type: DataTypes.STRING(100),
			allowNull: true,
			unique: true
		},
		price: {
			type: DataTypes.DECIMAL,
			allowNull: true
		},
		active: {
			type: DataTypes.INTEGER(1),
			allowNull: false,
			defaultValue: '1'
		},
		comment: {
			type: DataTypes.STRING(500),
			allowNull: true
		}
	}, {
		tableName: 'customer_package'
	});
};
