/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('log_mailjet', {
    id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    params: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    date_create: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'log_mailjet'
  });
};
