/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('convention', {
    id: {
      type: DataTypes.INTEGER(5).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    breakDuration: {
      type: DataTypes.TIME,
      allowNull: true
    },
    intervalBreak: {
      type: DataTypes.TIME,
      allowNull: true
    },
    medecinCheck: {
      type: DataTypes.DATE,
      allowNull: true
    },
    breakfastDuration: {
      type: DataTypes.TIME,
      allowNull: true
    },
    averageHoursWeek: {
      type: DataTypes.TIME,
      allowNull: true
    },
    maxHoursWeek: {
      type: DataTypes.TIME,
      allowNull: true
    },
    breakHeaven: {
      type: DataTypes.TIME,
      allowNull: true
    },
    daysOffPerMonth: {
      type: DataTypes.DATE,
      allowNull: true
    },
    reference: {
      type: DataTypes.STRING(80),
      allowNull: true
    },
    deleted: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    }
  }, {
    tableName: 'convention'
  });
};
