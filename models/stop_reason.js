/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('stop_reason', {
		id: {
			type: DataTypes.INTEGER(5).UNSIGNED,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		stop_id: {
			type: DataTypes.INTEGER(10).UNSIGNED,
			allowNull: true
		},
		l_stop_reason_id: {
			type: DataTypes.INTEGER(5).UNSIGNED,
			allowNull: true
		},
		comment: {
			type: DataTypes.STRING(120),
			allowNull: true
		}
	}, {
		tableName: 'stop_reason'
	});
};
