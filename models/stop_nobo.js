/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('stop_nobo', {
    id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    user_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
      references: {
        model: 'user',
        key: 'id'
      }
    },
    date_stop: {
      type: DataTypes.DATE,
      allowNull: true
    },
    come_back: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'stop_nobo'
  });
};
