/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('mp_unit', {
    id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    l_maintenanceType_id: {
      type: DataTypes.INTEGER(5).UNSIGNED,
      allowNull: true,
      references: {
        model: 'list',
        key: 'id'
      }
    },
    mp_template_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
      references: {
        model: 'mp_template',
        key: 'id'
      }
    }
  }, {
    tableName: 'mp_unit'
  });
};
