/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('company', {
    id: {
      type: DataTypes.INTEGER(5).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    corporate_name: {
      type: DataTypes.STRING(40),
      allowNull: true
    },
    business_name: {
      type: DataTypes.STRING(40),
      allowNull: true
    },
    siren: {
      type: DataTypes.INTEGER(9),
      allowNull: false
    },
    siret: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
    sap: {
      type: DataTypes.STRING(15),
      allowNull: true
    },
    address: {
      type: DataTypes.STRING(80),
      allowNull: true
    },
    zipcode: {
      type: DataTypes.STRING(12),
      allowNull: true
    },
    city: {
      type: DataTypes.STRING(40),
      allowNull: true
    },
    description: {
      type: DataTypes.STRING(120),
      allowNull: true
    },
    ceo_firstname: {
      type: DataTypes.STRING(40),
      allowNull: true
    },
    ceo_lastname: {
      type: DataTypes.STRING(40),
      allowNull: true
    },
    ceo_signature: {
      type: DataTypes.STRING(120),
      allowNull: true
    },
    logo: {
      type: DataTypes.STRING(100),
      allowNull: true
    }
  }, {
    tableName: 'company'
  });
};
