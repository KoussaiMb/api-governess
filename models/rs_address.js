/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('rs_address', {
    id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    rs_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false
    },
    lat: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    lng: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    radius: {
      type: DataTypes.INTEGER(4).UNSIGNED,
      allowNull: true
    },
    open_time: {
      type: DataTypes.TIME,
      allowNull: true
    },
    close_time: {
      type: DataTypes.TIME,
      allowNull: true
    }
  }, {
    tableName: 'rs_address'
  });
};
