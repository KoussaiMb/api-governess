/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('stop_raison', {
    id: {
      type: DataTypes.INTEGER(5).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    stop_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
      references: {
        model: 'stop_nobo',
        key: 'id'
      }
    },
    l_raison_id: {
      type: DataTypes.INTEGER(5).UNSIGNED,
      allowNull: true,
      references: {
        model: 'list',
        key: 'id'
      }
    },
    comment: {
      type: DataTypes.STRING(120),
      allowNull: true
    }
  }, {
    tableName: 'stop_raison'
  });
};
