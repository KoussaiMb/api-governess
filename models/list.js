/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('list', {
    id: {
      type: DataTypes.INTEGER(5).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    listName: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    field: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    position: {
      type: DataTypes.INTEGER(4).UNSIGNED,
      allowNull: true
    },
    deleted: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    }
  }, {
    tableName: 'list'
  });
};
