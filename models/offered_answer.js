/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('offered_answer', {
    id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    question_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
      references: {
        model: 'question',
        key: 'id'
      }
    },
    answer_text: {
      type: DataTypes.STRING(80),
      allowNull: true
    },
    position: {
      type: DataTypes.INTEGER(3).UNSIGNED,
      allowNull: true
    },
    score: {
      type: DataTypes.INTEGER(6),
      allowNull: true
    }
  }, {
    tableName: 'offered_answer'
  });
};
