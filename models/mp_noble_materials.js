/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('mp_noble_materials', {
    id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    address_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
      references: {
        model: 'address',
        key: 'id'
      }
    },
    l_noble_materials_id: {
      type: DataTypes.INTEGER(5).UNSIGNED,
      allowNull: true,
      references: {
        model: 'list',
        key: 'id'
      }
    },
    lr_room_id: {
      type: DataTypes.INTEGER(5).UNSIGNED,
      allowNull: true,
      references: {
        model: 'list_reference',
        key: 'id'
      }
    },
    comment: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    tableName: 'mp_noble_materials'
  });
};
