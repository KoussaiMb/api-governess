/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('field_reference', {
    id: {
      type: DataTypes.INTEGER(5).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    table_name: {
      type: DataTypes.STRING(20),
      allowNull: false
    },
    field_name: {
      type: DataTypes.STRING(20),
      allowNull: false
    },
    ref_name: {
      type: DataTypes.STRING(30),
      allowNull: false
    }
  }, {
    tableName: 'field_reference'
  });
};
