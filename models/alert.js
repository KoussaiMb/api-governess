/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('alert', {
    id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    created_by: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
      references: {
        model: 'user',
        key: 'id'
      }
    },
    priority_id: {
      type: DataTypes.INTEGER(5).UNSIGNED,
      allowNull: true,
      references: {
        model: 'list',
        key: 'id'
      }
    },
    title: {
      type: DataTypes.STRING(40),
      allowNull: true
    },
    description: {
      type: DataTypes.STRING(80),
      allowNull: true
    },
    url: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    check_count: {
      type: DataTypes.INTEGER(6),
      allowNull: true
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    finished: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    finished_at: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'alert'
  });
};
