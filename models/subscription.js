/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('subscription', {
    id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    token: {
      type: DataTypes.STRING(60),
      allowNull: true,
      unique: true
    },
    user_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
      references: {
        model: 'user',
        key: 'id'
      }
    },
    recurrence_id: {
      type: DataTypes.INTEGER(5).UNSIGNED,
      allowNull: true,
      references: {
        model: 'list',
        key: 'id'
      }
    },
    surface: {
      type: DataTypes.INTEGER(6),
      allowNull: true
    },
    workTime: {
      type: DataTypes.TIME,
      allowNull: true
    },
    workTimeChosen: {
      type: DataTypes.TIME,
      allowNull: true
    },
    date: {
      type: DataTypes.DATE,
      allowNull: true
    },
    price: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    promocode: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    promoPrice: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    firstname: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    lastname: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    email: {
      type: DataTypes.STRING(60),
      allowNull: true
    },
    phone: {
      type: DataTypes.STRING(13),
      allowNull: true
    },
    address: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    zipcode: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    payLater: {
      type: DataTypes.INTEGER(1),
      allowNull: true
    },
    payed: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    modified: {
      type: DataTypes.DATE,
      allowNull: true
    },
    modifiedNumber: {
      type: DataTypes.INTEGER(3).UNSIGNED,
      allowNull: false,
      defaultValue: '0'
    },
    checked: {
      type: DataTypes.DATE,
      allowNull: true
    },
    deleted: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    }
  }, {
    tableName: 'subscription'
  });
};
