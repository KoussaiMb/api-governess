/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('inspection', {
    id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    mission_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
      references: {
        model: 'mission',
        key: 'id'
      }
    },
    date: {
      type: DataTypes.DATE,
      allowNull: false
    },
    average_rate: {
      type: DataTypes.INTEGER(3),
      allowNull: false
    },
    grooming: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    comments: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    deleted: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '0'
    }
  }, {
    tableName: 'inspection'
  });
};
