/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('rs_request', {
		id: {
			type: DataTypes.INTEGER(10).UNSIGNED,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		rs_id: {
			type: DataTypes.INTEGER(10).UNSIGNED,
			allowNull: true
		},
		customer_id: {
			type: DataTypes.INTEGER(10).UNSIGNED,
			allowNull: true
		},
		requested_at: {
			type: DataTypes.DATE,
			allowNull: true
		},
		processed: {
			type: DataTypes.INTEGER(1),
			allowNull: false,
			defaultValue: '0'
		},
		processed_at: {
			type: DataTypes.DATE,
			allowNull: true
		},
		processed_by: {
			type: DataTypes.INTEGER(10).UNSIGNED,
			allowNull: true
		},
		issued: {
			type: DataTypes.INTEGER(1),
			allowNull: false,
			defaultValue: '0'
		},
		issue_comment: {
			type: DataTypes.STRING(255),
			allowNull: true
		},
		canceled: {
			type: DataTypes.INTEGER(1),
			allowNull: false,
			defaultValue: '0'
		},
		cancel_comment: {
			type: DataTypes.STRING(255),
			allowNull: true
		}
	}, {
		tableName: 'rs_request'
	});
};
