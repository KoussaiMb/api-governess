/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('event_user', {
    id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    event_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      references: {
        model: 'event',
        key: 'id'
      }
    },
    user_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
      references: {
        model: 'user',
        key: 'id'
      }
    },
    subtitle: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    subComment: {
      type: DataTypes.STRING(500),
      allowNull: true
    },
    available: {
      type: DataTypes.INTEGER(1),
      allowNull: true
    }
  }, {
    tableName: 'event_user'
  });
};
