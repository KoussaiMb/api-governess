/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('img_metro', {
    id: {
      type: DataTypes.INTEGER(3).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    path: {
      type: DataTypes.STRING(80),
      allowNull: false
    },
    line: {
      type: DataTypes.STRING(11),
      allowNull: false
    }
  }, {
    tableName: 'img_metro'
  });
};
