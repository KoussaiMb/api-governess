/* jshint ident: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('customer_address', {
		id: {
		    type: DataTypes.INTEGER(10).UNSIGNED,
		    allowNull: false,
		    primaryKey: true,
		    autoIncrement: true
		},
		address_id: {
		    type: DataTypes.INTEGER(10).UNSIGNED,
		    allowNull: true,
		    references: {
					model: 'user',
					key: 'id',
				}
		},
		customer_id: {
		    type: DataTypes.INTEGER(10).UNSIGNED,
		    allowNull: true,
		    references: {
					model: 'customer',
					key: 'id',
		    }
		},
		lockbox_code: {
		    type: DataTypes.STRING(40),
		    allowNull: true,
		},
		lockbox_localization: {
		    type: DataTypes.STRING(255),
		    allowNull: true,
		},
        trash_localization: {
            type: DataTypes.STRING(255),
            allowNull: true
        }
	}, {
	  tableName: 'customer_address'
	});
};
