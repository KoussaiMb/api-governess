/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('waiting_list', {
    id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    address_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
      references: {
        model: 'address',
        key: 'id'
      }
    },
    customer_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
      references: {
        model: 'customer',
        key: 'id'
      }
    },
    workflow_id: {
      type: DataTypes.INTEGER(3).UNSIGNED,
      allowNull: true,
      references: {
        model: 'workflow',
        key: 'id'
      }
    },
    date_list_in: {
      type: DataTypes.DATE,
      allowNull: true
    },
    date_list_out: {
      type: DataTypes.DATE,
      allowNull: true
    },
    accepted: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    pending: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    deleted: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    alerte: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '0'
    }
  }, {
    tableName: 'waiting_list'
  });
};
