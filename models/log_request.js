/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('log_request', {
    id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    scheme: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    host: {
      type: DataTypes.STRING(40),
      allowNull: true
    },
    path: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    method: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    method_original: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    media_type: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    body_params: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    ip_address: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    date_create: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'log_request'
  });
};
