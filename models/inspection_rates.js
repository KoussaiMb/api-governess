/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('inspection_rates', {
    id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    inspection_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
      references: {
        model: 'inspection',
        key: 'id'
      }
    },
    cardex_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
      references: {
        model: 'cardex',
        key: 'id'
      }
    },
    rate: {
      type: DataTypes.INTEGER(3),
      allowNull: false
    },
    deleted: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    }
  }, {
    tableName: 'inspection_rates'
  });
};
