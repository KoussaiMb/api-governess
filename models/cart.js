/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('cart', {
    id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    transaction_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
      references: {
        model: 'transaction',
        key: 'id'
      }
    },
    promocode_id: {
      type: DataTypes.INTEGER(5).UNSIGNED,
      allowNull: true,
      references: {
        model: 'promocode',
        key: 'id'
      }
    },
    item_id: {
      type: DataTypes.INTEGER(5).UNSIGNED,
      allowNull: true,
      references: {
        model: 'item',
        key: 'id'
      }
    },
    price: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    quantity: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '1.00'
    },
    discount_percentage: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    discount_euro: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    price_final: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    cart_count: {
      type: DataTypes.INTEGER(6),
      allowNull: false,
      defaultValue: '0'
    }
  }, {
    tableName: 'cart'
  });
};
