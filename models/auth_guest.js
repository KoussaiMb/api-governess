/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('auth_guest', {
    id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    api_token: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    api_token_expire: {
      type: DataTypes.DATE,
      allowNull: true
    },
    ip_address: {
      type: DataTypes.STRING(15),
      allowNull: true
    },
    browser: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    os: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    device: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '0'
    },
    brand: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    model: {
      type: DataTypes.STRING(30),
      allowNull: true
    }
  }, {
    tableName: 'auth_guest'
  });
};
