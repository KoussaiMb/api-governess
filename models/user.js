/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('user', {
    id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    user_token: {
      type: DataTypes.STRING(40),
      allowNull: true,
      unique: true
    },
    job_id: {
      type: DataTypes.INTEGER(5).UNSIGNED,
      allowNull: true,
      references: {
        model: 'list',
        key: 'id'
      }
    },
    group_id: {
      type: DataTypes.INTEGER(5).UNSIGNED,
      allowNull: true,
      references: {
        model: 'group_nobo',
        key: 'id'
      }
    },
    gender_id: {
      type: DataTypes.INTEGER(5).UNSIGNED,
      allowNull: true,
      references: {
        model: 'list',
        key: 'id'
      }
    },
    maritalStatus_id: {
      type: DataTypes.INTEGER(5).UNSIGNED,
      allowNull: true,
      references: {
        model: 'list',
        key: 'id'
      }
    },
    validated_by: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
      references: {
        model: 'user',
        key: 'id'
      }
    },
    who: {
      type: DataTypes.ENUM('customer','provider','bo','admin','guest','governess','guestrelation'),
      allowNull: true
    },
    firstname: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    lastname: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    email: {
      type: DataTypes.STRING(40),
      allowNull: true
    },
    phone: {
      type: DataTypes.STRING(15),
      allowNull: true
    },
    birthdate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    childNb: {
      type: DataTypes.INTEGER(3).UNSIGNED,
      allowNull: true
    },
    photo_profile: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    accesslevel: {
      type: DataTypes.INTEGER(2).UNSIGNED,
      allowNull: false,
      defaultValue: '0'
    },
    token: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    password: {
      type: DataTypes.STRING(80),
      allowNull: true
    },
    password_temp: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    password_asked_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    confirm_token: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    confirmed_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    reset_token: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    reset_asked_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    pending: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    paymentAccount: {
      type: DataTypes.STRING(40),
      allowNull: true
    },
    lastLog: {
      type: DataTypes.DATE,
      allowNull: true
    },
    validated_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    subscribed_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    deleted: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    deleted_at: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'user'
  });
};
