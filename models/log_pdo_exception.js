/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('log_pdo_exception', {
    id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    message: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    function: {
      type: DataTypes.STRING(80),
      allowNull: false
    },
    class: {
      type: DataTypes.STRING(30),
      allowNull: false
    },
    date_create: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'log_pdo_exception'
  });
};
