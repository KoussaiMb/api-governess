/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('maintenance', {
    id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    address_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
      references: {
        model: 'address',
        key: 'id'
      }
    },
    task_id: {
      type: DataTypes.INTEGER(5).UNSIGNED,
      allowNull: true,
      references: {
        model: 'task',
        key: 'id'
      }
    },
    type_task: {
      type: DataTypes.INTEGER(1),
      allowNull: true
    },
    task_time: {
      type: DataTypes.TIME,
      allowNull: true
    },
    position: {
      type: DataTypes.INTEGER(3).UNSIGNED,
      allowNull: true
    },
    deleted: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    }
  }, {
    tableName: 'maintenance'
  });
};
