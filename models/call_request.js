/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('call_request', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    action_history_id: {
      type: DataTypes.INTEGER(10),
      allowNull: false
    },
    request_date: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    request_time: {
      type: DataTypes.TIME,
      allowNull: true
    },
    l_call_request_id: {
      type: DataTypes.INTEGER(6),
      allowNull: false
    },
    done: {
      type: DataTypes.DATE,
      allowNull: true
    },
    canceled: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'call_request'
  });
};
