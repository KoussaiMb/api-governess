/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('question', {
    id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    l_input_type_id: {
      type: DataTypes.INTEGER(5).UNSIGNED,
      allowNull: true,
      references: {
        model: 'list',
        key: 'id'
      }
    },
    field_ref_id: {
      type: DataTypes.INTEGER(5).UNSIGNED,
      allowNull: true,
      references: {
        model: 'field_reference',
        key: 'id'
      }
    },
    survey_name: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    question_text: {
      type: DataTypes.STRING(80),
      allowNull: true
    },
    position: {
      type: DataTypes.INTEGER(3).UNSIGNED,
      allowNull: true
    },
    deleted: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    }
  }, {
    tableName: 'question'
  });
};
