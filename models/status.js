/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('status', {
    id: {
      type: DataTypes.INTEGER(5).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    HourlyWage: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    taxOnWage: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    grossSalary: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    netSalary: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    charge: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    deleted: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    }
  }, {
    tableName: 'status'
  });
};
