/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('menu_bo', {
    id: {
      type: DataTypes.INTEGER(5).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    url: {
      type: DataTypes.STRING(300),
      allowNull: true
    },
    menu_name: {
      type: DataTypes.STRING(40),
      allowNull: true
    },
    glyphicon: {
      type: DataTypes.STRING(60),
      allowNull: true
    },
    icon_url: {
      type: DataTypes.STRING(300),
      allowNull: true
    },
    deleted: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    }
  }, {
    tableName: 'menu_bo'
  });
};
