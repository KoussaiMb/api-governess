/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('refund', {
		id: {
			type: DataTypes.INTEGER(10).UNSIGNED,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		user_id: {
			type: DataTypes.INTEGER(10).UNSIGNED,
			allowNull: true
		},
		mission_id: {
			type: DataTypes.INTEGER(10).UNSIGNED,
			allowNull: true
		},
		l_refundReason_id: {
			type: DataTypes.INTEGER(5).UNSIGNED,
			allowNull: true
		},
		amount: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		transaction_id_stripe: {
			type: DataTypes.STRING(255),
			allowNull: false
		},
		deleted: {
			type: DataTypes.INTEGER(1),
			allowNull: false,
			defaultValue: '0'
		}
	}, {
		tableName: 'refund'
	});
};
