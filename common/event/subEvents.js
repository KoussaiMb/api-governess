const moment = require('moment');

function isValidated(missions, startSubEvent, endSubEvent){
    let exist = false
    missions.map(mission => {
        if(startSubEvent.isSame(moment(mission.dataValues.start)) && endSubEvent.isSame(moment(mission.dataValues.end))){
            exist = true
        }
    })
    return exist
}

function genNewEvent(start, event, frontId) {
    let subEvent = {
        id          :   event.id+'_'+frontId,
        mapId       :   event.id,
        token       :   event.token,
        title       :   event.title,
        start       :   event.start_time ? moment(`${start.format('YYYY-MM-DD')}T${event.start_time }`).format('YYYY-MM-DDTHH:mm:ss') : start.format('YYYY-MM-DD'),
        end         :   event.end_time   ? moment(`${start.add(event.duration,'days').format('YYYY-MM-DD')}T${event.end_time}`).format('YYYY-MM-DDTHH:mm:ss') : start.add(event.duration,'days').format('YYYY-MM-DD'),
        allDay      :   !!(!event.start_time && !event.end_time)
    }
    return Object.assign({}, subEvent, (event.type === 'benefit' && event.missions && moment(subEvent.end).isBefore(moment(),'second')) ? { validated : isValidated(event.missions, moment(subEvent.start), moment(subEvent.end))} : null)
}

function getWeek(date, weekNumberOnMonth, dayOnWeek){
  const endMonth      = moment(date).endOf('month');
  let   startMonth    = moment(date).startOf('month');
  let   _occurrence   = 0;
  let   _date         = moment(date);
  let   matched       = false;

  while(startMonth.isBetween(startMonth, endMonth, 'days', '[]') && !matched){
    if(startMonth.day() === dayOnWeek) {
      _occurrence += 1;
      _date = moment(startMonth)
    }
    if(_occurrence === weekNumberOnMonth){
      matched = true
    }
    startMonth = moment(startMonth).add(1, 'days')
  }
  return _date
}

function getFormatedPunctualEvents(event, userStartDate, userEndDate) {
    const callback = (frontId,startDate, subEvents) => {
        subEvents.push(genNewEvent(startDate, event))
    };
    return genSubEvents(moment(userStartDate), moment(userEndDate), event.start_rec, event.end_rec, null, 'days', callback,true)
}

function getFormatedDayEvents(event, userStartDate, userEndDate){
  const _modulo  = (moment(event.start_rec).date() % event.repeat_rec);
  const callback = (frontId, startDate, subEvents, modulo, startDateFixed, endDate, variant)=> {
    if (variant === 0)
        subEvents.push(genNewEvent(startDate, event, frontId))
  };
  return genSubEvents(userStartDate, userEndDate, event.start_rec, event.end_rec, _modulo, 'days', callback, false, event.repeat_rec)
}

function getFormatedWeekEvents(event, userStartDate, userEndDate){
  const _modulo  = (moment(event.start_rec).week() % event.repeat_rec);
  const callback = (frontId, startDate, subEvents, modulo, startDateFixed, endDate)=>{
    const _date = moment(startDate).day(event.day);
      if((startDate.week() % event.repeat_rec) === modulo && _date.isSameOrAfter(startDateFixed) && _date.isSameOrBefore(endDate))
          subEvents.push(genNewEvent(moment(startDate).day(event.day), event, frontId))
  };
  return genSubEvents(userStartDate, userEndDate, event.start_rec, event.end_rec, _modulo, 'weeks', callback)
}

function getFormatedMonthDayEvents(event, userStartDate, userEndDate){
  const _modulo  = (moment(event.start_rec).month() % event.repeat_rec);
  const callback = (frontId, startDate, subEvents, modulo)=>{
    if((startDate.month() % event.repeat_rec) === modulo)
      subEvents.push(genNewEvent(getWeek(startDate, event.nday, event.day), event, frontId))
  };
  return genSubEvents(userStartDate, userEndDate, event.start_rec, event.end_rec, _modulo, 'months', callback)
}

function getFormatedMonthDateEvents(event, userStartDate, userEndDate){
  const _modulo  = (moment(event.start_rec).month() % event.repeat_rec);
  const callback = (frontId, startDate, subEvents, modulo)=>{
    if((startDate.month() % event.repeat_rec) === modulo)
      subEvents.push(genNewEvent(startDate, event, frontId))
  };
  return genSubEvents(userStartDate, userEndDate, event.start_rec, event.end_rec, _modulo, 'months', callback)
}

function getFormatedYearEvents(event, userStartDate, userEndDate){
  const _modulo  = (moment(event.start_rec).year() % event.repeat_rec);
  const callback = (frontId, startDate, subEvents, modulo)=>{
    if((startDate.year() % event.repeat_rec) === modulo)
      subEvents.push(genNewEvent(startDate, event, frontId))
  };
  return genSubEvents(userStartDate, userEndDate, event.start_rec, event.end_rec, _modulo, 'years', callback)
}

function genSubEvents(_startDate, _endDate, startRec, endRec, modulo, addMode, callback, isPunctual=false, repeat_rec =false){
  let startDate  =  moment(_startDate);
  if(moment(startRec).isAfter(startDate))
    startDate  =  moment(startRec);
  const startDateFixed = startDate;
  let endDate    =  moment(_endDate);
  if(moment(endRec).isBefore(endDate))
    endDate    = moment(endRec);
  let _subEvents = [],
      notPunctual = true,
      frontId = 0,
      variant = 0
  while(startDate.isBetween(moment(_startDate), endDate, addMode, '[]') && notPunctual){
    callback(frontId, startDate, _subEvents, modulo, startDateFixed, endDate, variant);
    startDate  = moment(startDate).add(1, addMode)
    if(repeat_rec)
        variant = (variant+1)%repeat_rec
    if(isPunctual)
        notPunctual = !notPunctual
    frontId++
  }
  return _subEvents;
}

module.exports = function getSubEvents(event, userStartDate, userEndDate) {
  switch (event.repeat_type){
    case 'punctual':
      return getFormatedPunctualEvents(event, userStartDate, userEndDate);
      break;
    case 'day':
      return getFormatedDayEvents(event, userStartDate, userEndDate);
      break;
    case 'week':
      return getFormatedWeekEvents(event, userStartDate, userEndDate);
      break;
    case 'monthDay':
      return getFormatedMonthDayEvents(event, userStartDate, userEndDate);
      break;
    case 'monthDate':
      return getFormatedMonthDateEvents(event, userStartDate, userEndDate);
      break;
    case 'year':
      return getFormatedYearEvents(event, userStartDate, userEndDate);
      break;
    default :
      return []
  }
};
