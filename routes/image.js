let validatorErrorHandler = require("../middleware/validatorErrorHandler");
let express = require('express');
let models = require('../models');
let router = express.Router();
let Sequelize = require("sequelize");
const Op = Sequelize.Op;
let { getById } = require('../middleware/schemas');
let moment = require("moment");
var fs = require('fs');

router.get('/:pathToImg', (req, res, next) => {
  if (fs.existSync(process.env.HTTP_URL_BACKOFFICE_NOBO + '/' + req.params.pathToImg)) {
    res.sendFile(process.env.HTTP_URL_BACKOFFICE_NOBO + '/' + req.params.pathToImg);
  }
});

module.exports = router;