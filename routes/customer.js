let validatorErrorHandler = require("../middleware/validatorErrorHandler");
let express = require('express');
let models = require('../models');
let router = express.Router();
let Sequelize = require("sequelize");
const Op = Sequelize.Op;
let { getById } = require('../middleware/schemas');
let moment = require("moment");

router.get('/', validatorErrorHandler,
    (req, res, next) => {
        models.auth.findOne({
            where: {api_token: req.get("apiToken")}
        }).then(auth => {
            if (auth === null)
                throw({status: 404, message: "Customers not found"});
            return models.governess.findOne({where: {user_id: auth.user_id}});
        }).then(gov => {
            if (gov === null)
                throw({status: 404, message: "Customers not found"});
            return models.cluster.findOne({
                where: {governess_id: gov.id}
            });
        }).then(cluster => {
            if (cluster === null)
                throw({status: 404, message: "Customers not found"});
            return models.provider.findAll({
                where: {cluster_id: cluster.id}
            })
        }).then(provider => {
            if (provider === null)
                throw({status: 404, message: "Customers not found"});
            providerId = [];
            for (let i = 0; i < provider.length; i++) {
                providerId[i] = provider[i].id;
            }
            return models.agenda.findAll({
                where: {
                    deleted: 0, provider_id: providerId,
                    end_rec: {
                        [Op.or]: {
                            [Op.gte]: moment(),
                            [Op.eq]: null
                        }
                    }
                }
            })
        }).then(agenda => {
            if (agenda === null)
                throw({status: 404, message: "Customers not found"});
            userId = [];
            for (let i = 0; i < agenda.length; i++) {
                userId[i] = agenda[i].user_id;
            }
            return models.user.findAll({
                where: {id: userId},
                attributes: ['firstname', 'lastname', 'id', 'email', 'phone'],
                include: [{
                    model: models.customer, as: 'customer'
                }, {
                    model: models.address, as: 'address'
                }
                ]
            })
        }).then(data => {
            if (data === null)
                throw({status: 404, message: "Customers not found"});
            else {
                res.status(200).json({status: 200, message: "Customers successfully retrieved", data: data});
            }
        }).catch(err => next(err));
    }
);

router.get('/customers', validatorErrorHandler,
    (req, res, next) => {
        models.auth.findOne({
            where: {api_token: req.get("apiToken")}
        }).then(auth => {
            if (auth === null)
                throw({status: 404, message: "Customers not found"});
            return models.governess.findOne({where: {user_id: auth.user_id}});
        }).then(gov => {
            if (gov === null)
                throw({status: 404, message: "Customers not found"});
            return models.cluster.findOne({
                where: {governess_id: gov.id}
            });
        }).then(cluster => {
            if (cluster === null)
                throw({status: 404, message: "Customers not found"});
            return models.address_cluster.findAll({
                where: {cluster_id: cluster.id}
            })
        }).then(addressClusters => {
            if (addressClusters === null)
                throw({status: 404, message: "Customers not found"});
            let customersId = [];
            addressClusters.map( addressCluster => {
                customersId.push(addressCluster.dataValues.customer_id)
            })
            return models.customer.findAll({where : { id : {[Op.in]: customersId}}})
        }).then(customers => {
            if (customers === null)
                throw({status: 404, message: "Customers not found"});
            let usersId = []
            customers.map(customer => {
                usersId.push(customer.dataValues.user_id)
            })
            return models.user.findAll({
                where : {
                    id : { [Op.in] : usersId},
                    deleted : 0
                },
                attributes: ['id', 'firstname', 'lastname', 'email', 'phone', 'who']
            })
        }).then(users => {
            if (users === null)
                throw({status: 404, message: "Customers not found"});
            let data = []
            users.map(user => {
                data.push(user.dataValues)
            })
            res.status(200).json({status: 200, message: "Customers successfully retrieved", data: data})
        }).catch(err => next(err));
    }
);


router.get('/customersAddresses', validatorErrorHandler,
    (req, res, next) => {
        models.auth.findOne({
            where: {api_token: req.get("apiToken")}
        }).then(auth => {
            if (auth === null)
                throw({status: 404, message: "Customers not found"});
            return models.governess.findOne({where: {user_id: auth.user_id}});
        }).then(gov => {
            if (gov === null)
                throw({status: 404, message: "Customers not found"});
            return models.cluster.findOne({
                where: {governess_id: gov.id}
            });
        }).then(cluster => {
            if (cluster === null)
                throw({status: 404, message: "Customers not found"});
            return models.address_cluster.findAll({
                where: {cluster_id: cluster.id}
            })
        }).then(addressClusters => {
            if (addressClusters === null)
                throw({status: 404, message: "Customers not found"});
            let customersId = [];
            addressClusters.map( addressCluster => {
                customersId.push(addressCluster.dataValues.customer_id)
            })
            return models.customer.findAll({where : { id : {[Op.in]: customersId}}})
        }).then(customers => {
            if (customers === null)
                throw({status: 404, message: "Customers not found"});
            let usersId = []
            customers.map(customer => {
                usersId.push(customer.dataValues.user_id)
            })
            return models.user.findAll({
                where : {
                    id : { [Op.in] : usersId},
                    deleted : 0
                },
                attributes: ['id', 'firstname', 'lastname', 'email', 'phone', 'who', 'childNb'],
                include : [
                    {
                        model : models.customer,
                        as : 'customer',
                        required: true
                    },
                    {
                        model : models.address,
                        where : {deleted : 0},
                        as : 'address',
                        required: true
                    }
                ]
            })
        }).then(users => {
            if (users === null)
                throw({status: 404, message: "Customers not found"});
            let data = []
            users.map(user => {
                data.push(user.dataValues)
            })
            res.status(200).json({status: 200, message: "Customers successfully retrieved", data: data})
        }).catch(err => next(err));
    }
);

router.get('/address/:id', getById, validatorErrorHandler,
    (req, res, next) => {
        models.address.findAll({
            where: {user_id: req.params.id},
            include: {model: models.cardex, as: 'cardex', include: {model: models.list_reference, as: 'place'}}
        }).then(addresses => {
            if (addresses === null)
                throw({status: 404, message: "Address not found"});
            else
                res.status(200).json({status: 200, message: "Addresses successfully retrieved", data: addresses});
        }).catch(err => next(err))
    }
);

router.get('/:id', getById, validatorErrorHandler,
    (req, res, next) => {
        models.customer.findById(req.params.id).then(customer => {
            if (customer === null)
                throw ({status: 404, message: "Customer not found"});
            else
                res.status(200).json({status: 200, message: "Customer successfully retrieved", data: customer});
        })
            .catch(err => next(err))
    }
);

module.exports = router;
