let validatorErrorHandler = require("../middleware/validatorErrorHandler");
let express = require('express');
let models = require('../models');
let router = express.Router();
let sequelize = require("sequelize");
let moment = require("moment");
const op = sequelize.Op;
const crypto = require('crypto');
let {getById, eventUserUpdate } = require('../middleware/schemas');

router.put('/:id', getById, eventUserUpdate, validatorErrorHandler, (req, res, next) => {
    models.event_user.destroy({
        where: {event_id : req.params.id}
    }).then( deletedItem => {
        if (!deletedItem)
            throw ({status: 404, message: "Couldn't delete requested event_user"});
        return models.event_user.bulkCreate(req.body.eventUsers);
    }).then( createdItem => {
        if (!createdItem)
            throw ({status: 404, message: "Couldn't create requested event_user"});
        res.status(200).json({status: 200, message: "event_user successfully updated"});
    }).catch(err => next(err))
});



module.exports = router;
