let validatorErrorHandler = require("../middleware/validatorErrorHandler");
let express = require('express');
let models = require('../models');
let router = express.Router();
let Sequelize = require("sequelize");
const op = Sequelize.Op;
let { getById } = require('../middleware/schemas');
let moment = require("moment");

router.get('/',
    (req, res, next) => {
      models.auth.findOne({
        where: {api_token: req.get("apiToken")}
      }).then(auth => {
        if (auth === null)
          throw({status: 404, message: "authentication failed"});
          let who = [],
              whereStatement = {
                  deleted: 0
              }
          if (req.query.who !== undefined) {
              who = who.concat(req.query.who)
              whereStatement.who = {[op.in]: who}
          }
          return models.user.findAll({
              where: whereStatement,
              attributes: ['id', 'firstname', 'lastname', 'email', 'phone', 'who']
          })
      }).then(users => {
          if (users === null)
              throw({status: 404, message: "Users not found"});
          let data = []
          users.map( user => {
              data.push(user)
          })
          res.status(200).json({status: 200, message: "Customers successfully retrieved", data: data});
      }).catch(err => next(err));
    }
);


module.exports = router;
