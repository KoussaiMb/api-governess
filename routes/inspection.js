let validatorErrorHandler = require("../middleware/validatorErrorHandler");
let express = require('express');
let models = require('../models');
let router = express.Router();
let Sequelize = require("sequelize");
let moment = require("moment");
const Op = Sequelize.Op;
let { getById, addInspection, getInspection,
  inspectionSearch } = require('../middleware/schemas');

router.get('/page/:page', getInspection,
    validatorErrorHandler,
    (req, res, next) => {
      let limit = 20;
      let offset = 0;
      let pages = 0;
      let missionId = [];

      if (req.params.page <= 0)
        throw({status: 400, message: "Invalid page number"});

      models.auth.findOne({
        where: {api_token: req.get("apiToken")}
      }).then(auth =>  {
        if (auth === null)
          throw({status: 404, message: "Inspections not found"});
        return models.governess.findOne({
          where: {user_id: auth.dataValues.user_id}
        })
      }).then(governess => {
        if (governess === null)
          throw({status: 404, message: "Inspections not found"});
        return models.mission.findAll({
          where: {governess_id: governess.dataValues.id}
        })
      }).then(mission => {
        if (mission === null)
          throw({status: 404, message: "Inspections not found"});
        for (let i = 0; i < mission.length; i++)
          missionId[i] = mission[i].id;
        return models.inspection.findAndCountAll({where: {mission_id: missionId}})
            .then((data) => {
              let page = req.params.page;      // page number
              pages = Math.ceil(data.count / limit);
              offset = limit * (page - 1);
              return models.inspection.findAll({
                where: {mission_id: missionId},
                attributes: ['date', 'average_rate', 'id'],
                order: [['date', 'desc']],
                limit: limit,
                offset: offset,
                $sort: {id: 1},
                include: [{
                  model: models.mission, as: 'mission',
                  attributes: ['real_start', 'real_end', 'id', 'real_work_time'],
                  include: [{
                    model: models.user, as: 'customer',
                    attributes: ['firstname', 'lastname', 'id']
                  }, {
                    model: models.provider, as: 'provider',
                    attributes: ['id'],
                    include: [{
                      model: models.user, as: 'user',
                      attributes: ['firstname', 'lastname', 'id']}]
                  }]
                }]
              })
            })
      }).then(inspection => {
        if (inspection === null)
          throw({status: 404, message: "Inspections not found"});
        res.status(200).json({status: 200, message: "Inspections successfully retrieved", data: inspection, totalPages: pages});
      }).catch(err => next(err));
    }
);

// TODO: change doc, add provider_id as parameter
router.post('/', addInspection, validatorErrorHandler,
    (req, res, next) => {
      let inspections = [];

      models.auth.findOne({
        where: {api_token: req.get("apiToken")}
      }).then(auth =>  {
        if (auth === null)
          throw({status: 404, message: "Inspections not found"});
        return models.governess.findOne({
          where: {user_id: auth.dataValues.user_id}
        })
      }).then(governess => {
        if (governess === null)
          throw({status: 404, message: "Inspections not found"});
        return models.inspection.create({
          mission_id: req.body.mission_id,
          average_rate: req.body.average_rate,
          date: new Date(),
          grooming: req.body.grooming,
          comments: req.body.comments,
          provider_id: req.body.provider_id,
          governess_id: governess.dataValues.id
        }).then(inspection => {
          inspections = inspection;
          for (let i = 0; i < req.body.rates.length; i++)
            req.body.rates[i]['inspection_id'] = inspections.id;
          models.inspection_rates.bulkCreate(req.body.rates).then(() => {
            return models.inspection_rates.findAll();
          }).then(() => {
            return models.mission.update({inspected: 1}, {where: {id: req.body.mission_id}})
          }).then(inspection_rates => {
            res.status(200).json({status: 200, message: 'Inspection successfully added', data: inspection});
          });
        })
      }).catch(err => next(err));
    }
);

router.get('/search', inspectionSearch, validatorErrorHandler,
    (req, res, next) => {
      if (!req.get('inspectionDate') && !req.get('providerName') &&
          !req.get('clientName'))
        return next({status: 404, message: "Inspection not found"});
      models.inspection.findAll({
        where: (req.get('inspectionDate')) ?
            Sequelize.where(Sequelize.fn('date', Sequelize.col('date')),
                '=',
                req.get('inspectionDate')) : undefined,
        attributes: ['date', 'average_rate', 'id'],
        order: [['date', 'desc']],
        include: [{
          required: true,
          model: models.mission, as: 'mission',
          attributes: ['real_start', 'real_end', 'id', 'real_work_time'],
          include: [{
            required: true,
            model: models.user, as: 'customer',
            where: (req.get('clientName')) ? Sequelize.where(
                Sequelize.fn("concat",
                    Sequelize.col("mission->customer.firstname"),
                    Sequelize.col("mission->customer.lastname")),
                {[Op.like]: req.get('clientName')}) : undefined,
            attributes: ['firstname', 'lastname', 'id']
          }, {
            model: models.provider, as: 'provider',
            required: true,
            attributes: ['id'],
            include: [{
              required: true,
              model: models.user, as: 'user',
              where: (req.get('providerName')) ? Sequelize.where(
                  Sequelize.fn("concat",
                      Sequelize.col("mission->provider->user.firstname"),
                      Sequelize.col("mission->provider->user.lastname")),
                  {[Op.like]: req.get('providerName')}) : undefined,
              attributes: ['firstname', 'lastname', 'id']}]
          }]
        }]
      }).then(inspection => {
        if (inspection.length === 0)
          throw ({status: 404, message: "Inspection not found"});
        res.status(200).json({status: 200, message: "Inspections successfully retrieved", data: inspection});
      }).catch(err => next(err));
    }
);

router.get('/:id', getById, validatorErrorHandler,
    (req, res, next) => {
      models.inspection.findOne({
        where: {id :req.params.id},
        include: [{
          model: models.inspection_rates,
          attributes: ['rate', 'id'],
          as: 'rates',
          include: [{
            model: models.cardex,
            attributes: ['description'],
            as: 'task',
            include: [{
              model: models.list_reference,
              as: 'place',
              attributes: ['value']
            }]
          }]
        }]
      }).then(inspection => {
        if (inspection === null)
          throw ({status: 404, message: "Inspection not found"});
        else
          res.status(200).json({status: 200, message: "Inspection successfully retrieved", data: inspection});
      }).catch(err => next(err))
    }
);

module.exports = router;
