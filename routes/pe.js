let validatorErrorHandler = require("../middleware/validatorErrorHandler");
let express = require('express');
let models = require('../models');
let router = express.Router();
let sequelize = require("sequelize");
let moment = require("moment");
const op = sequelize.Op;
const crypto = require('crypto');
let hat = require('hat');
let { getById, listSearch, lockboxUpdate, listReferenceSearch } = require('../middleware/schemas');

router.get('/list/:listName', listSearch, validatorErrorHandler,
    (req, res, next) => {
        models.list.findAll({
            where : { listName : req.params.listName}
        }).then(list =>{
            if (list === null)
                throw ({status: 404, message: "list not found"});
            else
                res.status(200).json({status: 200, message: "List successfully retrieved", data: list});
        })
            .catch(err => next(err))
    }
);

router.get('/listReference/:listName', listReferenceSearch, validatorErrorHandler,
    (req, res, next) => {
        models.list_reference.findAll({
            where : { listName : req.params.listName}
        }).then(list =>{
            if (list === null)
                throw ({status: 404, message: "list not found"});
            else
                res.status(200).json({status: 200, message: "List Reference successfully retrieved", data: list});
        })
            .catch(err => next(err))
    }
);


// router.put('/:id', getById, lockboxUpdate, validatorErrorHandler, (req, res, next) => {
//     editLockbox(req, res, next, false);
// });
//
//
// function editLockbox(req, res, next) {
//             models.customer_address.update(req.body.lockbox_code, {where: {id : req.body.id}}).then(rows => {
//             if (rows[0] === 0)
//                 throw ({status: 404, message: "Event to update not found"});
//             res.status(200).json({status: 200, message: "Event successfully updated"});
//         }).catch(err => next(err));
// }

router.put('/:id', getById, validatorErrorHandler, (req, res, next) => {
    models.customer_address.update(req.body.lockbox_code, {where: {address_id: req.params.id}}).then(rows => {
        if (rows === 0)
            throw ({status: 500, message: "Couldn't validate mission"});
        res.status(200).json({status: 200, message: "Mission validated"});
    }).catch(err => next(err));
});



module.exports = router;
