let validatorErrorHandler = require("../middleware/validatorErrorHandler");
let express = require('express');
let models = require('../models');
let router = express.Router();
let Sequelize = require("sequelize");
let { dayMission, getById } = require('../middleware/schemas');
let moment = require("moment");
const Op = Sequelize.Op;

router.get('/today', validatorErrorHandler,
    (req, res, next) => {
      let today = new Date().getDay();
      let week = ((moment().week() + 1) % 2) + 1;

      models.auth.findOne({
        where: {api_token: req.get("apiToken")}
      }).then(auth => {
        if (auth === null)
          throw({status: 404, message: "Missions not found"});
        return models.governess.findOne({where: {user_id: auth.user_id}});
      }).then(gov => {
        if (gov === null)
          throw({status: 404, message: "Customers not found"});
        return models.cluster.findOne({where: {governess_id: gov.id}});
      }).then(cluster => {
        if (cluster === null)
          throw({status: 404, message: "Missions not found"});
        return models.provider.findAll({where: {cluster_id: cluster.id}})
      }).then(provider => {
        if (provider === null)
          throw({status: 404, message: "Missions not found"});
        providerId = [];
        for (let i = 0; i < provider.length; i++)
          providerId[i] = provider[i].id;
        return models.agenda.findAll({
          where: {
            deleted: 0, provider_id: providerId,
            type: [0, 3, week],
            start_rec: {[Op.lte]: moment()}, day: today,
            end_rec: {
              [Op.or]: {
                [Op.gte]: moment(),
                [Op.eq]: null
              }
            }
          },
          attributes: ['start', 'end', 'id'],
          include: [{
            model: models.provider, as: 'provider', attributes: ['user_id', 'id'],
            include: [{
              model: models.user, as: 'user', attributes: ['firstname', 'lastname']}
            ]},
            {
              model: models.user, as: 'customer', attributes: ['firstname', 'lastname']
            },
            {
              model: models.address, as: 'address', attributes: ['zipcode', 'address', 'mission_cur']
            }]
        })
      }).then(data => {
        if (data === null)
          throw({status: 404, message: "Missions not found"});
        else if (data.length === 0)
          res.status(200).json({status: 200, message: "No mission today"});
        else
          res.status(200).json({status: 200, message: "Missions successfully retrieved", data: data});
      }).catch(err => next(err))
    });

router.get('/day/:day', dayMission, validatorErrorHandler,
    (req, res, next) => {
      let week = ((moment().week() + 1) % 2) + 1;

      models.auth.findOne({
        where: {api_token: req.get("apiToken")}
      }).then(auth => {
        if (auth === null)
          throw({status: 404, message: "Missions not found"});
        return models.governess.findOne({where: {user_id: auth.user_id}});
      }).then(gov => {
        if (gov === null)
          throw({status: 404, message: "Customers not found"});
        return models.cluster.findOne({where: {governess_id: gov.id}});
      }).then(cluster => {
        if (cluster === null)
          throw({status: 404, message: "Missions not found"});
        return models.provider.findAll({
          where: {cluster_id: cluster.dataValues.id}
        })
      }).then(provider => {
        if (provider === null)
          throw({status: 404, message: "Missions not found"});
        providerId = [];
        for (let i = 0; i < provider.length; i++)
          providerId[i] = provider[i].id;
        return models.agenda.findAll({
          where: {
            deleted: 0, provider_id: providerId,
            type: [0, 3, week],
            start_rec: {[Op.lte]: moment()}, day: req.params.day
          },
          end_rec: {
            [Op.or]: {
              [Op.gte]: moment(),
              [Op.eq]: null
            }
          },
          attributes: ['start', 'end', 'id'],
          include: [{
            model: models.provider, as: 'provider', attributes: ['user_id'],
            include: [{model: models.user, as: 'user', attributes: ['firstname', 'lastname']}]
          }, {
            model: models.user, as: 'customer', attributes: ['firstname', 'lastname'],
            include: [{model: models.address, as: 'address', attributes: ['zipcode', 'address']}]
          }]
        })
      }).then(data => {
        if (data === null)
          throw({status: 404, message: "Missions not found"});
        else if (data.length === 0)
          res.status(200).json({status: 200, message: "No mission this day"});
        else
          res.status(200).json({status: 200, message: "Missions successfully retrieved", data: data});
      }).catch(err => next(err))
    });

router.get('/:id', getById, validatorErrorHandler,
    (req, res, next) => {
      models.agenda.findOne({
        where: {id: req.params.id},
        attributes: ['start', 'end', ['start_rec', 'startRec'], ['end_rec', 'endRec'], 'id'],
        include: [{
          model: models.user, as: 'customer', attributes: ['id', 'firstname', 'lastname', 'birthdate', 'phone', 'email', 'childNb']}, {
          model: models.address, as: 'address', attributes: ['zipcode', 'address', 'id']}, {
          model: models.provider, as: 'provider', attributes: ['id'], include: {
            model: models.user, as: 'user', attributes: ['id', 'firstname', 'lastname', 'birthdate', 'phone', 'email', 'childNb']}
        }],
      }).then(data => {
        if (data === null)
          throw ({status: 404, message: "Mission not found"});
        else
          res.status(200).json({status: 200, message: "Mission successfully retrieved", data: data});
      }).catch(err => next(err))
    }
);

module.exports = router;
