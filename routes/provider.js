let validatorErrorHandler = require("../middleware/validatorErrorHandler");
let express = require('express');
let models = require('../models');
let moment = require("moment");
let Op = require("sequelize").Op;
let router = express.Router();
let { getById } = require('../middleware/schemas');

router.get('/:id/week/next', getById, validatorErrorHandler,
  (req, res, next) => {
    let week = (moment().week() % 2) + 1;
    return models.agenda.findAll({
      where: {
        end_rec: {
          [Op.or]: {
            [Op.eq]: null,
            [Op.gt]: moment()
          }
        },
        type: [0, 3, week],
        provider_id: req.params.id
      },
      attributes: ['id', 'start', 'end', ['start_rec', 'startRec'], ['end_rec', 'endRec'], 'type', 'day'],
      include: {
        model: models.user, as: 'customer', attributes: ['id', 'firstname', 'lastname', 'email', 'phone', 'who'],
        include: {
          model: models.address, as: 'address', attributes: ['id', 'address', 'zipcode']
        }
      },
      order: [['day', 'ASC']]
    })
      .then(data => {
        if (data === null)
          throw ({status: 404, message: "Missions not found"});
        else
          res.status(200).json({status: 200, message: "Missions successfully retrieved", data: data});
      })
      .catch(err => next(err))
  });

router.get('/:id/week/current', getById, validatorErrorHandler,
  (req, res, next) => {
    let week = ((moment().week() + 1) % 2) + 1;
    return models.agenda.findAll({
      where: {
        end_rec: {
          [Op.or]: {
            [Op.gte]: moment(),
            [Op.eq]: null
          }
        },
        type: [0, 3, week],
        provider_id: req.params.id
      },
      attributes: ['id', 'start', 'end', ['start_rec', 'startRec'], ['end_rec', 'endRec'], 'type', 'day'],
      include: {
        model: models.user, as: 'customer', attributes: ['firstname', 'lastname', 'id'],
        include: {
          model: models.address, as: 'address', attributes: ['id', 'address', 'zipcode']
        }
      },
      order: [['day', 'ASC']]
    })
      .then(data => {
        if (data === null)
          throw ({status: 404, message: "Missions not found"});
        else
          res.status(200).json({status: 200, message: "Missions successfully retrieved", data: data});
      })
      .catch(err => next(err))
  });

router.get('/', validatorErrorHandler,
    (req, res, next) => {
      models.auth.findOne({
        where: {api_token: req.get("apiToken")}
      }).then(auth => {
        if (auth === null)
          throw({status: 404, message: "Providers not found"});
        return models.governess.findOne({
          where: {user_id: auth.dataValues.user_id}
        })
      }).then(governess => {
        if (governess === null)
          throw({status: 404, message: "governess not found"});
        return models.cluster.findOne({
          where: {governess_id : governess.dataValues.id}
        })
      }).then(cluster => {
        if (cluster === null)
          throw({status: 404, message: "cluster not found"});
        return models.provider.findAll({
          where: {cluster_id: cluster.dataValues.id},
          attributes: ['id', 'hoursLeft', 'convention_id'],
          include: [{
            model: models.user, as: 'user', attributes: ['id', 'firstname', 'lastname', 'email', 'phone', 'who']
          }]
        })
      }).then(provider => {
        if (provider === null)
          throw({status: 404, message: "Providers not found"});
        else
          res.status(200).json({status: 200, message: "Providers successfully retrieved", data: provider});
      }).catch(err => next(err));
    }
);

router.get('/:id', getById, validatorErrorHandler,
    (req, res, next) => {
        models.provider.findById(req.params.id).then(provider => {
            if (provider === null)
                throw ({status: 404, message: "Provider not found"});
            else
                res.status(200).json({status: 200, message: "Provider successfully retrieved", data: provider});
        })
            .catch(err => next(err))
    }
);

module.exports = router;
