let validatorErrorHandler = require("../middleware/validatorErrorHandler");
let express = require('express');
let models = require('../models');
let Sequelize = require("sequelize");
let router = express.Router();
const Op = Sequelize.Op;
let { getById } = require('../middleware/schemas');

// TODO : add to DOCUMENTATION
router.get('/:id', getById, validatorErrorHandler,
  (req, res, next) => {
    models.mp_cardex.findOne({
      where: {l_maintenanceType_id: req.params.id},
    }).then(mp_cardex => {
      if (mp_cardex === null)
        throw({status: 404, message: "Cardex not found"});
      return models.mp_cardex_task.findAll({
        where: {mp_cardex_id: mp_cardex.id},
        include: [
          {model: models.list, as: "task"},
          {model: models.list_reference, as: "place"}
        ]
      });
    }).then((task) => {
      if (task === null)
        throw({status: 404, message: "Task not found"});
      res.status(200).json({status: 200, message: "Cardex task successfully retrieved", data: task});
      }
    ).catch(err => next(err));
});

module.exports = router;