let validatorErrorHandler = require("../middleware/validatorErrorHandler");
let express = require('express');
let models = require('../models');
let Sequelize = require("sequelize");
let router = express.Router();
let { address } = require('../middleware/schemas');
const Op = Sequelize.Op;
let { getById } = require('../middleware/schemas');
let moment = require("moment");

router.get('/byCluster', validatorErrorHandler,
    (req, res, next) => {
        models.auth.findOne({
            where: {api_token: req.get("apiToken")}
        }).then(auth => {
            if (auth === null)
                throw({status: 404, message: "address not found"});
            return models.governess.findOne({where: {user_id: auth.user_id}});
        }).then(gov => {
            if (gov === null)
                throw({status: 404, message: "address not found"});
            return models.cluster.findOne({
                where: {governess_id: gov.id}
            });
        }).then(cluster => {
            if (cluster === null)
                throw({status: 404, message: "address not found"});
            return models.address_cluster.findAll({
                where: {cluster_id: cluster.id}
            })
        }).then(addressClusters => {
            if (addressClusters === null)
                throw({status: 404, message: "address not found"});
            let addressesId = [];
            addressClusters.map( addressCluster => {
                addressesId.push(addressCluster.dataValues.address_id)
            })
            return models.address.findAll({
                where : { id : {[Op.in]: addressesId}},
                attributes: ['id', 'lat', 'lng', 'address', 'address_ext','city', 'country', 'zipcode'],
                include:[{
                    model : models.user,
                    as : 'user',
                    attributes : ['id'],
                    include : [
                        {
                            model : models.customer,
                            as  : 'customer',
                            attributes : ['id']
                        }
                    ]
                }]
            })
        }).then(addresses => {
            if (addresses === null)
                throw({status: 404, message: "address not found"});
            let data = []
            addresses.map(address => {
                data.push(Object.assign({}, address.dataValues,{ customer : address.dataValues.user.customer.dataValues}))
            })
            res.status(200).json({status: 200, message: "Addresses successfully retrieved", data: data})
        }).catch(err => next(err));
    }
);

router.get('/', validatorErrorHandler,
    (req, res, next) => {
        models.auth.findOne({
            where: {api_token: req.get("apiToken")}
        }).then(auth => {
            if (auth === null)
                throw({status: 404, message: "Addresses not found"});
            return models.governess.findOne({where: {user_id: auth.user_id} });
        }).then(gov => {
            if (gov === null)
                throw({status: 404, message: "Addresses not found"});
            return models.cluster.findOne({
                where: {governess_id: gov.id}
            })
        }).then(cluster => {
            if (cluster === null)
                throw({status: 404, message: "Addresses not found"});
            return models.provider.findAll({
                where: {cluster_id: cluster.dataValues.id}
            })
        }).then(provider => {
            if (provider === null)
                throw({status: 404, message: "Addresses not found"});
            providerId = [];
            for (let i = 0; i < provider.length; i++) {
                providerId[i] = provider[i].id;
            }
            return models.agenda.findAll({
                where: {
                    deleted: 0,
                    provider_id: providerId,
                    end_rec: {
                        [Op.or]: {
                            [Op.gte]: moment(),
                            [Op.eq]: null
                        }
                    }
                }
            })
        })
            .then(agenda => {
                if (agenda === null)
                    throw({status: 404, message: "Addresses not found"});
                addressId = [];
                for (let i = 0; i < agenda.length; i++)
                    addressId[i] = agenda[i].address_id;
                return models.address.findAll({
                    where: {id: addressId},
                    attributes: ['zipcode', 'address', 'id'],
                    include: [{
                        model: models.user, as: 'user', attributes: ['firstname', 'lastname', 'id']
                    }]
                })
            })
            .then(data => {
                if (data === null)
                    throw({status: 404, message: "Addresses not found"});
                else {
                    res.status(200).json({status: 200, message: "Addresses successfully retrieved", data: data});
                }
            })
            .catch(err => next(err));
    }
);

// TODO : add to DOCUMENTATION
router.get('/planning/:id', getById, validatorErrorHandler,
    (req, res, next) => {
        models.address.findById(req.params.id)
            .then(address => {
                if (address === null)
                    throw({status: 404, message: "Address not found"});
                else
                    return models.maintenance.findAll({
                        where: {
                            address_id: req.params.id,
                            position: address.dataValues.maintenance_position,
                            delmaintenance_positioneted: 0
                        },
                        include: {
                            model: models.task, as: 'task'
                        }
                    })
            })
            .then(data => {
                if (data === null)
                    throw({status: 404, message: "No planning found for this address"});
                else
                    res.status(200).json({status: 200, message: "Planning successfully retrieved", data: data});
            })
            .catch(err => next(err));
    }
);

// TODO : add to DOCUMENTATION
router.get('/planningv2/:id', getById, validatorErrorHandler,
    (req, res, next) => {
        models.mp_address.belongsTo(models.mp_template, {foreignKey: 'mp_template_id'})
        models.mp_template.hasMany(models.mp_address, {foreignKey: 'mp_template_id'})

        models.mp_unit.belongsTo(models.mp_template, {foreignKey: 'mp_template_id'})
        models.mp_template.hasMany(models.mp_unit, {foreignKey: 'mp_template_id'})

        models.mp_unit_task.belongsTo(models.mp_unit, {foreignKey: 'mp_unit_id'})
        models.mp_unit.hasMany(models.mp_unit_task, {foreignKey: 'mp_unit_id'})

        models.mp_template.belongsTo(models.list, {foreignKey: 'l_recurrence_id'})
        models.list.hasMany(models.mp_template, {foreignKey: 'l_recurrence_id'})

        models.mp_cardex_task.belongsTo(models.mp_cardex, {foreignKey: 'mp_cardex_id'})
        models.mp_cardex.hasMany(models.mp_cardex_task, {foreignKey: 'mp_cardex_id'})

        models.mp_unit_task.belongsTo(models.list, {foreignKey: 'l_rollingTask_id'})
        models.list.hasMany(models.mp_unit_task, {foreignKey: 'l_rollingTask_id'})

        models.mp_cardex.belongsTo(models.mp_unit, {foreignKey: 'l_maintenanceType_id'})
        models.mp_unit.hasMany(models.mp_cardex, {foreignKey: 'l_maintenanceType_id'})

        models.mp_unit.belongsTo(models.list, {foreignKey: 'l_maintenanceType_id'})
        models.list.hasMany(models.mp_unit, {foreignKey: 'l_maintenanceType_id'})

        models.mp_cardex_task.belongsTo(models.mp_cardex, {foreignKey: 'mp_cardex_id'})
        models.mp_cardex.hasMany(models.mp_cardex_task, {foreignKey: 'mp_cardex_id'})

        models.mp_cardex.belongsTo(models.list, {foreignKey: 'l_maintenanceType_id'})
        models.list.hasMany(models.mp_cardex, {foreignKey: 'l_maintenanceType_id'})

        models.mp_cardex_task.belongsTo(models.list, {foreignKey: 'l_task_id'})
        models.list.hasMany(models.mp_cardex_task, {foreignKey: 'l_task_id'})

        models.mp_cardex_task.belongsTo(models.list_reference, {foreignKey: 'lr_room_id'})
        models.list_reference.hasMany(models.mp_cardex_task, {foreignKey: 'lr_room_id'})


        models.mp_address.findOne({
            where : { address_id : req.params.id},
            include : [
                {
                    model : models.mp_template,
                    include: [
                        {
                            model: models.list,
                            as: 'list',
                            attributes: ['field']
                        },
                        {
                            model : models.mp_unit,
                            include : [
                                {
                                    model : models.mp_unit_task,
                                    include : [
                                        {
                                            model: models.list,
                                            as: 'list',
                                            attributes: ['field']
                                        }
                                    ]
                                },
                                {
                                    model: models.list,
                                    as: 'list',
                                    attributes: ['field']
                                },
                            ]
                        }
                    ]
                }
            ]
        }).then(mp_addresses => {
            // console.log(mp_addresses)
            if (mp_addresses === null)
                res.status(200).json({status: 200, message: "Address not found", data : {}});
            res.status(200).json({status: 200, message: "Planning successfully retrieved", data: mp_addresses});
        })
            .catch(err => next(err));
    }
);

// TODO : add to DOCUMENTATION
router.get('/product/:id', getById, validatorErrorHandler,
    (req, res, next) => {
        models.product_localization.findAll({
            where: {address_id: req.params.id},
            include : [{model: models.list_reference, as: 'product'}, {model: models.list_reference, as: 'room'}]
        })
            .then(data => {
                if (data === null)
                    throw({status: 404, message: "No product found for this address"});
                else
                    res.status(200).json({status: 200, message: "Products successfully retrieved", data: data});
            })
            .catch(err => next(err));
    }
);

// TODO : add to DOCUMENTATION
router.get('/material/:id', getById, validatorErrorHandler,
    (req, res, next) => {
        models.mp_noble_materials.belongsTo(models.mp_address, {foreignKey: 'address_id'})
        models.mp_address.hasMany(models.mp_noble_materials, {foreignKey: 'address_id'})

        models.mp_noble_materials.belongsTo(models.list_reference, {foreignKey: 'lr_room_id'})
        models.list_reference.hasMany(models.mp_noble_materials, {foreignKey: 'lr_room_id'})

        models.mp_noble_materials.belongsTo(models.list, {foreignKey: 'l_noble_materials_id'})
        models.list.hasMany(models.mp_noble_materials, {foreignKey: 'l_noble_materials_id'})

        models.mp_noble_materials.findAll({
            where : { address_id : req.params.id},
            include : [{
                model : models.mp_address
            },
                {
                    model: models.list,
                    as: 'list',
                    attributes: ['field']
                },
                {
                    model: models.list_reference,
                    as: 'list_reference',
                    attributes: ['value', 'url']
                }
            ]
        }).then(mp_materials => {
            if (mp_materials === null)
                res.status(200).json({status: 200, message: "No materials found for this address", data : {}});
            res.status(200).json({status: 200, message: "materials successfully retrieved", data: mp_materials});
        })
            .catch(err => next(err));
    }
);

// TODO : add to DOCUMENTATION
router.get('/mp_cardex/:id', getById, validatorErrorHandler,
    (req, res, next) => {
        models.mp_cardex_task.belongsTo(models.mp_cardex, {foreignKey: 'mp_cardex_id'})
        models.mp_cardex.hasMany(models.mp_cardex_task, {foreignKey: 'mp_cardex_id'})

        models.mp_cardex.belongsTo(models.list, {foreignKey: 'l_maintenanceType_id'})
        models.list.hasMany(models.mp_cardex, {foreignKey: 'l_maintenanceType_id'})

        models.mp_cardex_task.belongsTo(models.list, {foreignKey: 'l_task_id'})
        models.list.hasMany(models.mp_cardex_task, {foreignKey: 'l_task_id'})

        models.mp_cardex_task.belongsTo(models.list_reference, {foreignKey: 'lr_room_id'})
        models.list_reference.hasMany(models.mp_cardex_task, {foreignKey: 'lr_room_id'})

        models.mp_cardex.findAll({
            where : { l_maintenanceType_id : req.params.id,  deleted: 0 },
            include : [{
                model: models.list,
                as: 'list',
                attributes: ['field']
            },
                {
                    model : models.mp_cardex_task,
                    include : [
                        {
                            model: models.list_reference,
                            as: 'list_reference',
                            attributes: ['value']
                        },
                        {
                            model: models.list,
                            as: 'list',
                            attributes: ['field']
                        },
                    ]
                }
            ]
        }).then(mp_cardex => {
            if (mp_cardex === null)
                res.status(200).json({status: 200, message: "No cardex found for this address", data : {}});
            res.status(200).json({status: 200, message: "cardex successfully retrieved", data: mp_cardex});

        })
            .catch(err => next(err));
    }
);

router.get('/:id', getById, validatorErrorHandler,
    (req, res, next) => {
        models.address.findById(req.params.id).then(address => {
            if (address === null)
                throw ({status: 404, message: "Address not found"});
            else
                res.status(200).json({status: 200, message: "Address successfully retrieved", data: address});
        })
            .catch(err => next(err))
    }
);

// TODO : add to DOCUMENTATION
router.get('/localizationLockboxTrash/:id', getById, validatorErrorHandler,
    (req, res, next) => {

        models.customer_address.belongsTo(models.address, {foreignKey: 'address_id'})
        models.address.hasMany(models.customer_address, {foreignKey: 'address_id'})

        models.customer_address.findAll({
            where : { address_id : req.params.id}
        }).then(customer_address => {
            if (customer_address === null)
                res.status(200).json({status: 200, message: "No materials found for this address", data : {}});
            res.status(200).json({status: 200, message: "materials successfully retrieved", data: customer_address});
        })
            .catch(err => next(err));
    }
);

module.exports = router;
