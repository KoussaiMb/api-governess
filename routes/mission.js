let validatorErrorHandler = require("../middleware/validatorErrorHandler");
let express = require('express');
let models = require('../models');
let router = express.Router();
let Sequelize = require("sequelize");
let { getById, missionCreate } = require('../middleware/schemas');
let moment = require("moment");
let hat = require('hat');
const Op = Sequelize.Op;

router.get('/', validatorErrorHandler,
    (req, res, next) => {
      models.auth.findOne({
        where: {api_token: req.get("apiToken")}
      }).then(auth =>  {
        if (auth === null)
          throw({status: 404, message: "Missions not found"});
        return models.governess.findOne({
          where: {user_id: auth.dataValues.user_id}
        })
      }).then(governess => {
        if (governess === null)
          throw({status: 404, message: "Missions not found"});
        return models.mission.findAll({
          where: {governess_id: governess.dataValues.id}
        })
      }).then(data => {
        if (data === null)
          throw({status: 404, message: "Missions not found"});
        else if (data.length === 0)
          res.status(200).json({status: 200, message: "No mission"});
        else
          res.status(200).json({status: 200, message: "Missions successfully retrieved",
            data: data});
      }).catch(err => next(err))
    });


router.post('/', missionCreate, validatorErrorHandler, (req, res, next) => {
    models.auth.findOne({
        where: {api_token: req.get("apiToken")}
    }).then(auth =>  {
        if (auth === null)
            throw({status: 404, message: "governess not found"});
        return models.governess.findOne({
            where: {user_id: auth.dataValues.user_id}
        })
    }).then(governess => {
        if (governess === null)
            throw({status: 404, message: "governess not found"});
        let mission = req.body.mission
        mission.governess_id = governess.dataValues.id
        if(!mission.mission_token)
            mission.mission_token  = hat(77, 16);
        if(!mission.real_start)
            mission.real_start = mission.start;
        if(!mission.real_end)
            mission.real_end = mission.end
        let diff = moment(mission.end).diff(mission.start, 'm')
        let diffR = moment(mission.real_end).diff(mission.real_start, 'm')
        mission.work_time       = moment(`${Math.trunc(diff / 60)}:${Math.trunc(diff % 60)}:00`, 'HH:mm:ss').format('HH:mm:ss')
        mission.real_work_time  = moment(`${Math.trunc(diffR / 60)}:${Math.trunc(diffR % 60)}:00`, 'HH:mm:ss').format('HH:mm:ss')
        return models.mission.create(req.body.mission)
    }).then(missionCreated => {
        console.log(missionCreated.dataValues)
        if (!missionCreated)
            throw ({status: 500, message: "Couldn't create mission"});
        res.status(200).json({status: 200, message: "mission created"});
    }).catch(err => next(err));
});

//TODO : add to documentation
router.get('/:id/cardex', getById, validatorErrorHandler,
(req, res, next) => {
  models.mission.findById(req.params.id).then(mission => {
    if (mission === null)
    throw({status: 404, message: "Missions not found"});
    return models.cardex.findAll({where: {address_id: mission.dataValues.address_id},
      include : {model: models.list_reference, as: "place"}})
    }).then(cardex => {
      if (cardex === null)
      throw({status: 404, message: "Missions not found"});
      res.status(200).json({status: 200, message: "Cardex successfully retrieved", data: cardex});
    }).catch(err => next(err));
  });

  // TODO: add to documentation
  router.get('/getMp_address_task_id', validatorErrorHandler,
  (req, res, next) => {
    models.auth.findOne({
      where: {api_token: req.get("apiToken")}
    }).then(auth =>  {
      if (auth === null)
        throw({status: 404, message: "user not found"});
      return models.auth.findOne({where: {user_id: auth.dataValues.user_id},
        attributes: ['user_id']})
        .then(user_id => {
            if (user_id === null)
              throw({status: 404, message: "User_id not found"});
            else
            models.address.findOne({where: {user_id: user_id.dataValues.user_id},
              attributes: ['id']})
          .then(address_id => {
            if (address_id === null)
              throw({status: 404, message: "address_id not found"});
            else
              models.mp_address.findOne({where: {address_id: address_id.dataValues.id},
            attributes: ['id']})
            .then(mp_address_id => {
              if (mp_address_id === null)
                throw({status: 404, message: "mp_address_id not found"});
              else
              models.mp_address_task.findOne({where: {mp_address_id: mp_address_id.dataValues.id},
              attributes: ['id']})
            .then(id => {
              if (id === null)
              throw({status: 404, message: 'id not found :)'});
              res.status(200).json({status: 200, message: "Cardex successfully retrieved", data: id});
            })
          })
        })
      })
    }).catch(err => (console.log(err)));
  })

  //TODO : add to documentation
  router.get('/:id/mp_address_task', getById, validatorErrorHandler,
  (req, res, next) => {
    models.mp_address_task.findById(req.params.id).then(address => {
      if (address === null)
      throw({status: 404, message: "cardex not found"});
      return models.mp_address_task.findAll({where: {mp_address_id: address.dataValues.mp_address_id},
          include: [
            {model: models.mp_address, as: 'mp_address', attributes: ["mp_template_id"],
             include: [{model: models.mp_template, as: 'template_id', attributes: ["l_recurrence_id"],
                include: [{model: models.list, as: 'recurrence_id', attributes: ["field", "deleted"]}
              ]},
            ]},
            {model: models.mp_unit, as: 'unit_id', attributes: ['l_maintenanceType_id', 'mp_template_id'],
               include: [{model: models.list, as: 'maintenance_type', attributes: ['field', 'deleted']}]
            },
            {model: models.list, as: 'rollingTask_id', attributes: ['field','deleted']},
          ],
      }).then(cardex => {
        if (cardex === null)
          throw({status: 404, message: "Missions not found"});
        else
        res.status(200).json({status: 200, message: "Missions successfully retrieved", data: cardex});
      }).catch(err => next(err))
    })
  })

//TODO : add to documentation
  router.get('/:id/getMaintenancePosition', getById, validatorErrorHandler,
  (req, res, next) => {
    models.mp_address_task.findById(req.params.id).then(address => {
      if (address === null)
        throw({status: 404, message: 'address not found'});
      return models.mp_address_task.findOne({where: {mp_address_id: address.dataValues.mp_address_id},
      }).then(address_id => {
        if (address_id === null)
          throw({status: 404, message: 'address not found'});
        else
          models.mp_address.findOne({where: {id: address_id.dataValues.mp_address_id}, attributes: ['address_id']})
          .then(address_id => {
            if (address_id === null)
              throw ({ status: 404, message: "position not found" });
            else
              models.address.findOne({ where: { id: address_id.dataValues.address_id}, attributes: ['maintenance_position']})
              .then(maintenance_position => {
                if  (maintenance_position === null)
                  throw({status: 404, message: 'maintenance position not found'})
                res.status(200).json({ status: 200, message: "position successfully retrieved", data: maintenance_position });
            })
          })
          })
    }).catch(err => (console.log(err)));
  })

//TODO : add to documentation
router.get('/:id/l_maintenanceType_id', getById, validatorErrorHandler,
(req, res, next) => {
  models.mission.findById(req.params.id).then(misisons => {
    if (mission === null)
    throw({status: 404, message: "Missions not found"});
    return models.mp_address_task.findAll({where: {mp_address_id: mission.dataValues.mp_address_task},
      attributes: ['mp_unit_id'],
      include: [{model: models.mp_unit, as: 'mp_unit', attributes: ['l_maintenanceType_id']}]
    }).then(cardex => {
      if (cardex === null)
        throw({status: 404, message: "Missions not found"});
      else
      res.status(200).json({status: 200, message: "Missions successfully retrieved", data: cardex});
    }).catch(err => next(err))
  })
}
)

router.put('/validate/:id', getById, validatorErrorHandler, (req, res, next) => {
  let govId = null;
  models.auth.findOne({where: {api_token: req.get('apiToken')},
    include: {
      required: true,
      model: models.user,
      as: 'user',
      include: {
        required: true,
        model: models.governess,
        as: 'governess'
      }}
  }).then(auth => {
    if (auth === null)
      throw ({status: 404, message: "Mission not found"});
    govId = auth.user.governess.id;
    return models.mission.findById(req.params.id);
  }).then(mission => {
    if (mission === null || mission.governess_id !== govId)
      throw ({status: 404, message: "Mission not found"});
    return models.mission.update({
      real_start: mission.start,
      real_end: mission.end,
      real_work_time: mission.work_time,
      finished: 1
    }, {where: {id: req.params.id}});
  }).then(rows => {
    if (rows === 0)
      throw ({status: 500, message: "Couldn't validate mission"});
    res.status(200).json({status: 200, message: "Mission validated"});
  }).catch(err => next(err));
});

router.get('/ongoing', validatorErrorHandler,
    (req, res, next) => {
      models.auth.findOne({
        where: {api_token: req.get("apiToken")}
      }).then(auth =>  {
        if (auth === null)
          throw({status: 404, message: "Missions not found"});
        return models.governess.findOne({
          where: {user_id: auth.dataValues.user_id}
        })
      }).then(governess => {
        if (governess === null)
          throw({status: 404, message: "Missions not found"});
        return models.mission.findAll({
          where: {
            governess_id: governess.dataValues.id, inspected: 0,
            start: {[Op.lte]: new Date()},
            end: {[Op.gte]: moment(new Date()).add(30, 'm').toDate()},
          },
          attributes: ['id', 'start', 'end'],
          include: [
            {model: models.provider, as: 'provider', attributes: ['user_id', 'id'],
              include: [{model: models.user, as: 'user', attributes: ['firstname', 'lastname']}]
            },
            {model: models.user, as: 'customer', attributes: ['firstname', 'lastname']},
            {model: models.address, as: 'address', attributes: ['zipcode', 'address', 'lng', 'lat']}
          ],
          order: [['end', 'ASC']]
        })
      }).then(data => {
        if (data === null)
          throw({status: 404, message: "Missions not found"});
        else if (data.length === 0)
          res.status(200).json({status: 200, message: "No on going mission"});
        else {
          res.status(200).json({status: 200, message: "Missions successfully retrieved", data: data});
        }
      }).catch(err => next(err))
    });

router.get('/:id', getById, validatorErrorHandler,
    (req, res, next) => {
      models.mission.findById(req.params.id).then(mission => {
        if (mission === null)
          throw ({status: 404, message: "Mission not found"});
        else
          res.status(200).json({status: 200, message: "Mission successfully retrieved", data: mission});
      }).catch(err => next(err))
    }
);

module.exports = router;
