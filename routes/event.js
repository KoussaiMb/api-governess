let validatorErrorHandler = require("../middleware/validatorErrorHandler");
let express = require('express');
let models = require('../models');
let router = express.Router();
let sequelize = require("sequelize");
let moment = require("moment");
const op = sequelize.Op;
const crypto = require('crypto');
let { eventGet, eventCreate, getById, eventUpdate, eventDelete } = require('../middleware/schemas');
let getSubEvents = require('../common/event/subEvents');
let hat = require('hat');
let eventsUserIds = []

router.get('/', eventGet, validatorErrorHandler, (req, res, next) => {
    models.event.hasMany(models.mission, {foreignKey: 'event_id'})
  let users = [],
      types   = []
  let whereStatement = {
    start_rec   : {[op.lte]: moment(req.get('endDate')).toDate()},
    end_rec     : {[op.gte]: moment(req.get('startDate')).toDate()}
  }

  if (req.query.users !== undefined)
      users = users.concat(req.query.users);
  if (req.query.types !== undefined) {
      types = types.concat(req.query.types);
      whereStatement.type = {[op.in]: types};
  }
  models.event.findAll({
    where: whereStatement,
    include: [
        {
           model: models.event_user, as: 'event_users',
           required: true,
           attributes: ['id', 'subtitle', 'subComment', 'available'],
           include: [{
                   required: true,
                   model: models.user, as: 'user',
                   attributes: ['id', 'firstname', 'lastname', 'email', 'phone', 'who']
           }]
        },
        {
            model: models.address, as: 'address',
            attributes: ['id', 'lat', 'lng', 'address', 'address_ext', 'zipcode', 'city', 'country']
        },
        {
            model: models.mission,
            required: false,
            where: {
                end     : {[op.lt] : moment().toDate()},
                start   : {[op.gte]: moment(req.get('startDate')).toDate()}
            }
        }
    ]
  }).then(events => {
    let eventsList = [];
    if (events.length === 0)
      throw ({status: 200, message: "No events found", data: []});
    new Promise((resolve, reject) => {
      events.forEach(event => {
        event.dataValues.subEvents = getSubEvents(event, req.get('startDate'), req.get('endDate'));
          delete event.dataValues.missions
        if (users.length !== 0 && event.event_users.some(eventUser => users.includes(eventUser.user.id.toString())))
          eventsList.push(event.dataValues);
      });
      if (users.length !== 0)
        resolve(eventsList);
      else
        resolve(events);
    }).then(eventsList => {
      if (eventsList.length === 0)
        throw ({status: 200, message: "No events found", data: []});
      res.status(200).json({status: 200, message: "Events successfully retrieved", data: eventsList});
    }).catch(err => next(err));
  }).catch(err => next(err));
});

router.post('/', eventCreate, validatorErrorHandler, (req, res, next) => {
    req.body.event.token = hat(77, 16);
    models.event.create(req.body.event).then(eventCreated => {
        if (!eventCreated)
            throw ({status: 500, message: "Couldn't create event"});
        req.body.users.forEach(user => {
            user.event_id = eventCreated.id;
        });
        return models.event_user.bulkCreate(req.body.users);
    }).then(eventUsers => {
        if (eventUsers.length === 0)
            throw ({status: 500, message: "Couldn't create event users"});
        res.status(200).json({status: 200, message: "Event created"});
    }).catch(err => next(err));
});

router.put('/:id', getById, eventUpdate, validatorErrorHandler, (req, res, next) => {
  switch (req.body.op) {
    case "allSubEvents":
      modifyEvent(req, res, next, false);
      break;
    case "allNextSubEvents":
      splitEvent(req, res, next, false, false);
      break;
    case "uniqueEvent":
      splitEvent(req, res, next, true, false);
      break;
  }
});

router.delete('/:id', getById, eventDelete, validatorErrorHandler, (req, res, next) => {
  switch (req.body.op) {
    case "allSubEvents":
      modifyEvent(req, res, next, true);
      break;
    case "allNextSubEvents":
      splitEvent(req, res, next, false, true);
      break;
    case "uniqueEvent":
      splitEvent(req, res, next, true, true);
      break;
  }
});

function createEvent(event, next, token, isUnique = false) {
  let srcId = event.id;
  let createdId = null;
  //Delete property we do not want to be updated
  delete event.id
  event.token = token
  if(event.newToken){
      event.token = hat(77, 16)
  }
  delete event.newToken
  if (isUnique)
    event.repeat_type = 'punctual'

  models.event.create(event).then(eventCreated => {
    if (!eventCreated)
      throw ({status: 500, message: "Couldn't create requested event"});
    createdId = eventCreated.id
    return models.event_user.findAll({where: {event_id: srcId}});
  }).then(eventUsers => {
      //console.log('eventsUserIds',eventsUserIds)
      //console.log('eventUsers',eventUsers)
    if (eventUsers.length === 0 && eventsUserIds.length === 0)
      throw ({status: 500, message: "Event has no event users"});
    let cleanEventUserArray = [],
        _eventUsers         = eventsUserIds
    if(eventUsers.length !== 0)
        eventUsers.map( eventUser => {
            let _eventUser = eventUser.dataValues
            _eventUsers.push(_eventUser)
        })
      _eventUsers.forEach(eventUser => {
          delete eventUser.id;
          eventUser.event_id =  createdId
          cleanEventUserArray.push(eventUser)
    })
      eventsUserIds = []
    return models.event_user.bulkCreate(cleanEventUserArray);
  }).then(eventUsersCreated => {
    if (eventUsersCreated.length === 0)
      throw ({status: 500, message: "Error while creating event users"});
  }).catch(err => next(err));
}

function deleteEvent(event, res, next, sendResponse, NotSafe = true, startDate = false) {
    let whereStatement = {};
    if (startDate)
        whereStatement.start_rec = {[op.gte]: startDate};
    if(event.id)
        whereStatement.id = event.id;
    if(event.token)
        whereStatement.token = event.token
    models.event.findAll({where: whereStatement}).then( events => {
        if (!events)
            throw ({status: 404, message: "Couldn't delete requested event"});
        let eventsIds = [];
        events.forEach(event => eventsIds.push(event.id));
        return models.event_user.findAll({where: {event_id: {[op.in]: eventsIds}}});
    }).then( event_users => {
        if (!event_users)
            throw ({status: 404, message: "Couldn't delete requested event"});
        eventsUserIds      = [];
        let _eventsUserIds = []
        event_users.forEach(event_user => {
            delete event_user.event_id
            if(!NotSafe)
                eventsUserIds.push(event_user)
            _eventsUserIds.push(event_user.id)
        });
        return models.event_user.destroy({where: {id: {[op.in]: _eventsUserIds}}});
    }).then(deletedItems => {
        if (!deletedItems)
            throw ({status: 404, message: "Couldn't delete requested event"});
        return models.event.destroy({where: whereStatement});
    }).then(deletedItems => {
        if (!deletedItems)
            throw ({status: 404, message: "Couldn't delete requested event"});
        if (sendResponse === true)
            res.status(200).json({status: 200, message: "Event successfully deleted"});
    }).catch(err => next(err))
}


function modifyEvent(req, res, next, eventDelete) {
  if (eventDelete)
    deleteEvent({ token : req.body.oldSubEvent.token }, res, next, eventDelete, true);
  else {
    delete req.body.event.id
    models.event.update(req.body.event, {where: {token : req.body.oldSubEvent.token}}).then(rows => {
      if (rows[0] === 0)
        throw ({status: 404, message: "Event to update not found"});
      res.status(200).json({status: 200, message: "Event successfully updated"});
    }).catch(err => next(err));
  }
}

function updateEvent(start, end, req, next, event) {
  let updateStartEnd = {};
  if (start !== null)
    updateStartEnd.start_rec = shiftDate(start, event.repeat_type, event.repeat_rec, 'add', event);
  if (end !== null)
    updateStartEnd.end_rec = shiftDate(end, event.repeat_type, event.repeat_rec, 'substract', event);
  models.event.update(updateStartEnd,
      {where: {id: req.params.id}}).then(rows => {
    if (rows[0] === 0)
      throw ({status: 404, message: "Event to update not found"});
  }).catch(err => next(err));
}

function shiftDate(date, repeatType, repeatRec, addOrSub, event=null) {
    let _repeatType = repeatType
    if (['monthDay', 'monthDate'].includes(repeatType))
        repeatType = 'month';
    if (addOrSub === 'add'){
        if(_repeatType === 'monthDay')
            return getWeek(moment(date).add(repeatRec, repeatType), event.nday, event.day);
        return moment(date).add(repeatRec, repeatType);
    }
    if(_repeatType === 'monthDay')
        return getWeek(moment(date).subtract(repeatRec, repeatType), event.nday, event.day);
    return moment(date).subtract(repeatRec, repeatType);
}

function compareDate(subEventDate, eventDate, repeatRec, repeatType, isStart) {
  if (['monthDay', 'monthDate'].includes(repeatType))
    repeatType = 'month';
  if (repeatType === 'day' && repeatRec === 1)
    return moment(subEventDate).isSame(eventDate, 'day');
  if (isStart){
      return moment(subEventDate).isBetween(eventDate,moment(eventDate).add(repeatRec, repeatType), repeatType, '[)');
  }
  return moment(subEventDate).isBetween(eventDate, moment(eventDate).subtract(repeatRec, repeatType), repeatType, '(]');
}

function splitEvent(req, res, next, isUnique, eventDelete) {
  let repeat_rec = req.body.oldSubEvent.repeat_rec;
  let repeat_type = req.body.oldSubEvent.repeat_type;
  models.event.findById(req.params.id).then(event => {
      if(event.repeat_type === 'punctual'){
          deleteEvent({id: event.id}, res, next);
      }
      if (isUnique) {
        if(event.repeat_type !== 'punctual'){
            if (compareDate(req.body.oldSubEvent.start, event.end_rec, repeat_rec, repeat_type, false) === false && compareDate(req.body.oldSubEvent.start, event.start_rec, repeat_rec, repeat_type, true) === false)
            {
                console.log('HELLO_1')
                event.dataValues.start_rec = shiftDate(req.body.oldSubEvent.start, repeat_type, repeat_rec, 'add', event);
                createEvent(event.dataValues, next, event.token);
            }
            if(compareDate(req.body.oldSubEvent.start, event.end_rec, repeat_rec, repeat_type, false) && compareDate(req.body.oldSubEvent.start, event.start_rec, repeat_rec, repeat_type, true))
            {
                console.log('HELLO_2')
                deleteEvent({id: event.id}, res, next)
            }
            else if(compareDate(req.body.oldSubEvent.start, event.start_rec, repeat_rec, repeat_type, true))
            {
                console.log('HELLO_3')
                updateEvent(req.body.oldSubEvent.start, null, req, next, event)
            }
            else
            {
                console.log('HELLO_4')
                updateEvent(null, req.body.oldSubEvent.start, req, next, event);
            }
        }
      if (!eventDelete)
      {
          console.log('eventDelete_1')
          createEvent(req.body.event, next, event.token, true);
      }
      res.status(200).json({
        status: 200,
        message: "Event successfully " + (eventDelete === true ? "deleted" : "updated")
      });
    }
    else {
      models.event.count({where: {token: event.token}}).then(count => {
          console.log('compare', req.body.oldSubEvent.start, event.start_rec)
          if (compareDate(req.body.oldSubEvent.start, event.start_rec, repeat_rec, repeat_type, true)){
            deleteEvent({id: req.params.id}, res, next, eventDelete);
        }
        else
        {
            console.log('updateEvent', req.body.oldSubEvent)
            updateEvent(null, req.body.oldSubEvent.start, req, next, event);
        }
        if (eventDelete && count > 1)
            deleteEvent({token : event.token}, res, next, false, eventDelete, req.body.oldSubEvent.start);
        else if (!eventDelete)
        {
            //console.log('createEvent', req.body.event)
            createEvent(req.body.event, next, event.token);
        }
        res.status(200).json({
          status: 200,
          message: "Event successfully " + (eventDelete === true ? "deleted" : "updated")
        });
      });
    }
  }).catch(err => next(err));
}

function getWeek(date, weekNumberOnMonth, dayOnWeek){
    const endMonth      = moment(date).endOf('month');
    let   startMonth    = moment(date).startOf('month');
    let   _occurrence   = 0;
    let   _date         = moment(date);
    let   matched       = false;

    while(startMonth.isBetween(startMonth, endMonth, 'days', '[]') && !matched){
        if(startMonth.day() === dayOnWeek) {
            _occurrence += 1;
            _date = moment(startMonth)
        }
        if(_occurrence === weekNumberOnMonth){
            matched = true
        }
        startMonth = moment(startMonth).add(1, 'days')
    }
    return _date
}

module.exports = router;
