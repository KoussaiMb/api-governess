let {check} = require('express-validator/check');
let moment = require('moment');

exports.address = [
  check('address').exists().isString(),
  check('city').exists().isString(),
  check('zipcode').exists().isInt(),
  check('country').exists().isString()
];

exports.customer = [
  check('user_id').exists().isInt(),
];

exports.dayMission = [
  check('day').exists().isInt()
];

exports.getById = [
  check('id').exists().isInt()
];

exports.getInspection = [
  check('page').exists().isInt()
];

exports.addInspection = [
  check('mission_id').exists().isInt(),
  check('average_rate').exists().isInt(),
  check('rates').exists().isArray(),
  check('grooming').exists().isInt(),
  check('comments').exists()
];

exports.inspectionSearch = [
  check('clientName'),
  check('providerName'),
  check('inspectionDate')
];

exports.eventGet = [
  check('startDate').exists().custom(value => moment(value, "YYYY-MM-DD").isValid()),
  check('endDate').exists().custom(value => moment(value, "YYYY-MM-DD").isValid())
];

exports.eventCreate = [
  check('event').exists(),
  check('event.title').exists(),
  check('event.type').exists(),
  check('event.address_id').exists(),
  check('event.start_rec').exists().custom(value => moment(value, "YYYY-MM-DD").isValid()),
  check('event.end_rec').exists().custom(value => moment(value, "YYYY-MM-DD").isValid()),
  check('event.start_time').exists().custom(value => moment(value, "HH:mm:ss").isValid()),
  check('event.end_time').exists().custom(value => moment(value, "HH:mm:ss").isValid()),
  check('event.repeat_type').exists().isIn(['day','week','monthDate','monthDay','year','punctual']),
  check('users').exists().isArray()
];

exports.eventUpdate = [
  check('op').exists().isIn(['allSubEvents', 'allNextSubEvents', 'uniqueEvent']),
  check('oldSubEvent').exists(),
  //check('oldSubEvent.start_rec').custom(value => moment(value, "YYYY-MM-DD").isValid()),
  //check('oldSubEvent.end_rec').custom(value => moment(value, "YYYY-MM-DD").isValid()),
  //check('oldSubEvent.start_time').custom(value => moment(value, "HH:mm:ss").isValid()),
  //check('oldSubEvent.end_time').custom(value => moment(value, "HH:mm:ss").isValid()),
  //check('oldSubEvent.repeat_type').exists(),
  //check('oldSubEvent.repeat_rec').exists(),

  check('event').exists(),
  check('event.address_id').exists().isInt(),
  check('event.duration').exists().isInt(),
  check('event.type').exists().isIn(['benefit', 'availability', 'firstAppointment', 'appointment', 'other']),
  check('event.start_time').custom(value => moment(value, "HH:mm:ss").isValid()),
  check('event.end_time').custom(value => moment(value, "HH:mm:ss").isValid()),
  //check('event.end_rec').custom(value => moment(value, "YYYY-MM-DD").isValid())
];

exports.eventDelete = [
  check('op').exists().isIn(['allSubEvents', 'allNextSubEvents', 'uniqueEvent']),

  check('oldSubEvent').exists(),
  check('oldSubEvent.start_rec').custom(value => moment(value, "YYYY-MM-DD").isValid()),
  check('oldSubEvent.end_rec').custom(value => moment(value, "YYYY-MM-DD").isValid()),
  check('oldSubEvent.start_time').custom(value => checkTime(value)),
  check('oldSubEvent.end_time').custom(value => checkTime(value)),
  check('oldSubEvent.repeat_type').exists(),
  check('oldSubEvent.repeat_rec').exists(),
];
exports.eventUserUpdate = [
  check('eventUsers').exists().isArray()
];

exports.missionCreate = [
    check('mission').exists(),
    check('mission.provider_id').exists().isInt(),
    check('mission.user_id').exists().isInt(),
    check('mission.address_id').exists().isInt(),
    check('mission.start').custom(value => moment(value).isValid()),
    check('mission.end').custom(value => moment(value).isValid())
];

function checkTime(time) {
  if (time === null)
    return true;
  return moment(time, "HH:mm:ss").isValid();
}