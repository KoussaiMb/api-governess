const { validationResult } = require('express-validator/check');

function validatorErrorHandler(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty())
  {
    let error = errors.array({onlyFirstError: true})[0];
    if (error.value === undefined)
      return res.status(422).json({status: 422, message: "Missing parameter [" + error.param + "] in [" + error.location + "]"});
    return res.status(400).json({status: 400, message: "Invalid parameter [" + error.param + "] in [" + error.location + "]"});
  }
  next();
}

module.exports = validatorErrorHandler;