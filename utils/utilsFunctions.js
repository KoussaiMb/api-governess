module.exports = function removeJSONString(obj, toDelete) {
  // store all keys of this object for later use
  let keys = Object.keys(obj);
  // for each key update the "json" key
  keys.map(key => {
    // updates only if it has "json"
    if (obj[key].hasOwnProperty(toDelete)) {
      // assign the current obj a new field with "json" value pair
      Object.assign(obj[key], obj[key][toDelete]);
      console.log(obj);
      // delete "json" key from this object
      delete obj[key][toDelete];
    }
  })
  // updated all fields of obj
  return obj;
}

