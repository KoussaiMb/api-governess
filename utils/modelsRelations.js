let models = require('../models');

models.address.belongsTo(models.user, {foreignKey: 'user_id', as: 'user'});
models.user.hasMany(models.address, {foreignKey: 'user_id', as: 'address'});

models.agenda.belongsTo(models.provider, {foreignKey: 'provider_id', as: 'provider'});
models.provider.hasOne(models.agenda, {foreignKey: 'provider_id', as: 'agenda'});

models.provider.belongsTo(models.user, {foreignKey: 'user_id', as: 'user'});
models.user.hasOne(models.provider, {foreignKey: 'user_id', as: 'provider'});

models.governess.hasOne(models.cluster, {foreignKey: 'governess_id', as: 'cluster'});

models.governess.belongsTo(models.user, {foreignKey: 'user_id', as: 'user'});
models.user.hasOne(models.governess, {foreignKey: 'user_id', as: 'governess'});

models.auth.belongsTo(models.user, {foreignKey: 'user_id', as: 'user'});
models.user.hasOne(models.auth, {foreignKey: 'user_id', as: 'auth'});

models.auth.belongsTo(models.governess, {foreignKey: 'user_id', targetKey: 'user_id',
					 as: 'governess'});
models.governess.belongsTo(models.auth, {foreignKey: 'user_id', targetKey: 'user_id',
					 as: 'auth'});

models.mission.belongsTo(models.provider, {foreignKey: 'provider_id', as: 'provider'});
models.provider.hasOne(models.mission, {foreignKey: 'provider_id', as: 'mission'});

models.mission.belongsTo(models.user, {foreignKey: 'user_id', as: 'customer'});
models.user.hasOne(models.mission, {foreignKey: 'user_id', as: 'mission'});

models.agenda.belongsTo(models.user, {foreignKey: 'user_id', as: 'customer'});
models.user.hasOne(models.agenda, {foreignKey: 'user_id', as: 'agenda'});

models.customer.belongsTo(models.user, {foreignKey: 'user_id', as: 'user'});
models.user.hasOne(models.customer, {foreignKey: 'user_id', as: 'customer'});

models.inspection.belongsTo(models.mission, {foreignKey: 'mission_id', as: 'mission'});
models.mission.hasOne(models.inspection, {foreignKey: 'mission_id', as: 'inspection'});

models.mission.belongsTo(models.address, {foreignKey: 'address_id', as: 'address'});
models.address.hasOne(models.mission, {foreignKey: 'address_id', as: 'mission'});

models.agenda.belongsTo(models.address, {foreignKey: 'address_id', as: 'address'});
models.address.hasOne(models.agenda, {foreignKey: 'address_id', as: 'agenda'});

models.maintenance.belongsTo(models.task, {foreignKey: 'task_id', as: 'task'});
models.task.hasOne(models.maintenance, {foreignKey: 'task_id', as: 'maintenance'});

models.product_localization.belongsTo(models.list_reference, {foreignKey: 'lr_product_id', as: 'product'});

models.product_localization.belongsTo(models.list_reference, {foreignKey: 'lr_room_id', as: 'room'});

models.inspection_rates.belongsTo(models.inspection, {foreignKey: 'inspection_id',
  as: 'inspection_rate'});
models.inspection.hasMany(models.inspection_rates, {foreignKey: 'inspection_id',
  as: 'rates'});

models.event_user.belongsTo(models.event, {foreignKey: 'event_id', as: 'event'});
models.event.hasMany(models.event_user, {foreignKey: 'event_id', as: 'event_users'});

models.event.belongsTo(models.address, {foreignKey: 'address_id', as: 'address'});
models.address.hasOne(models.event, {foreignKey: 'address_id', as: 'event'});

models.event_user.belongsTo(models.user, {foreignKey: 'user_id', as: 'user'});
models.user.hasMany(models.event_user, {foreignKey: 'user_id', as: 'event'});

//PE CARDEX
models.mp_address_task.belongsTo(models.mp_address, {foreignKey: 'mp_address_id', as: 'mp_address'});
models.mp_address.hasMany(models.mp_address_task, {foreignKey: 'mp_address_id', as: 'mp_address'});

models.mp_address_task.belongsTo(models.mp_unit, {foreignKey: 'mp_unit_id', as: 'unit_id'});
models.mp_unit.hasMany(models.mp_address_task, {foreignKey: 'mp_unit_id', as: 'unit_id'});

models.mp_address_task.belongsTo(models.list, {foreignKey: 'l_rollingTask_id', as: 'rollingTask_id'});
models.list.hasMany(models.mp_address_task, {foreignKey: 'l_rollingTask_id', as: 'rollingTask_id'});

models.mp_address.belongsTo(models.mp_template, {foreignKey: 'mp_template_id', as: 'template_id'});
models.mp_template.hasMany(models.mp_address, {foreignKey: 'mp_template_id', as: 'template_id'});

models.mp_template.belongsTo(models.list, {foreignKey: 'l_recurrence_id', as: 'recurrence_id'});
models.list.hasMany(models.mp_template, {foreignKey: 'l_recurrence_id', as: 'recurrence_id'});

models.mp_unit.belongsTo(models.list, {foreignKey: 'l_maintenanceType_id', as: 'maintenance_type'});
models.list.hasMany(models.mp_unit, {foreignKey: 'l_maintenanceType_id', as: 'maintenance_type'});

models.mp_address.belongsTo(models.address, {foreignKey: 'address_id', as: 'addressID'});
models.address.hasMany(models.mp_address, {foreignKey: 'address_id', as: 'addressID'})
//

//Auth to mp_address_task_id
models.address.belongsTo(models.mp_address, {foreignKey: 'id', as: 'addressId'});
models.mp_address.hasOne(models.address, {foreignKey: 'id', as: 'addressId'});

models.mp_address.belongsTo(models.mp_address_task, {foreignKey: 'id', as: 'mp_addressId'});
models.mp_address_task.hasOne(models.mp_address, {foreignKey: 'id', as: 'mp_addressId'});
//
models.mp_cardex_task.belongsTo(models.list, {foreignKey: 'l_task_id',
  as: 'task'});
models.list.hasOne(models.mp_cardex_task, {foreignKey: 'l_task_id',
  as: 'task'});

models.mp_cardex_task.belongsTo(models.list_reference, {foreignKey: 'lr_room_id',
  as: 'place'});
models.list_reference.hasOne(models.mp_cardex_task, {foreignKey: 'lr_room_id',
  as: 'place'});
