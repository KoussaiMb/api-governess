const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const compression = require('compression');
const helmet = require('helmet');
const app = express();
const models = require('./models');
const address = require('./routes/address');
const mission = require('./routes/mission');
const provider = require('./routes/provider');
const customer = require ('./routes/customer');
const agenda = require ('./routes/agenda');
const inspection = require('./routes/inspection');
const event = require('./routes/event');
const event_user = require('./routes/event_user');
const image = require('./routes/image');
const user = require('./routes/user');
const mp_cardex = require('./routes/mp_cardex');
const logger = require('morgan');
const cors = require('cors');

require('./utils/modelsRelations');

// ** MIDDLEWARES **

if (app.get('env') === 'development')
    app.use(logger('dev'));
else if (app.get('env') === 'production')
    setupProductionLogStream();
app.disable('etag');
app.use(helmet());
app.use(cors());
app.use(compression());
app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));

// ** ROUTES **

app.use('/v2/governess/inspection', inspection);
app.use('/v2/governess/address', address);
app.use('/v2/governess/mission', mission);
app.use('/v2/governess/event', event);
app.use('/v2/governess/event_user', event_user);
app.use('/v2/governess/img', image);
app.use('/v2/governess/provider', provider);
app.use('/v2/governess/customer', customer);
app.use('/v2/governess/agenda', agenda);
app.use('/v2/governess/user', user);
app.use('/v2/governess/mp_cardex', mp_cardex);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  let err = new Error('Route not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  res.status(err.status || 500);

  if (err.status === 404)
  {
    // respond with json
    if (req.accepts('json')) {
      res.send({ status: 404, message: err.message || 'Route not found' });
      return;
    }

    // default to plain-text. send()
    res.type('txt').send('404 Route not found');
    return;
  }
  console.log("Server error : " + err.message);
  res.send({ status: err.status || 500, message: 'An unexpected error occured' });
});

console.log("Governess api listening on " + (process.env.GOVERNESS_PORT || 3005));
if (app.get('env') !== 'test')
    app.listen(process.env.GOVERNESS_PORT || 3005);
else
    module.exports = app;

function setupProductionLogStream()
{
  const rfs = require('rotating-file-stream');
  const fs = require('fs');
  const logDirectory = path.join(__dirname, 'logs');
  //Check if directory exists
  fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory);

  // create a rotating write stream
  const accessLogStream = rfs('access.log', {
    interval: '1d', // rotate daily
    path: logDirectory
  });
  app.use(logger('combined', {stream: accessLogStream}));
}
